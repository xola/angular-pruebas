function asyncAppBootstrap() {
    (function(window, TRAVIOOR) {
        "use strict";
        window.l = new _B.Logger("l", TRAVIOOR.loggerLevel);
        window.app = angular.module("app", ["ngRoute", "ngCookies", "ng", "LocalStorageModule", "ui.bootstrap", "angulartics", "angulartics.google.analytics", "slick", "matchMedia", "promise-queue", "ngProgress", "ngMap", "rzModule", "tmh.dynamicLocale", "ngSanitize", "credit-cards", "initialValue"])
    })(window, TRAVIOOR);
    (function(app, TRAVIOOR) {
        "use strict";
        app.constant("consts", Object.freeze({
            destinations_autocomplete: TRAVIOOR.destinationsAutocomplete,
            airports_autocomplete: TRAVIOOR.airportsAutocomplete,
            cities_autocomplete: TRAVIOOR.citiesAutocomplete,
            get_events_as_html: TRAVIOOR.getEventsAsHtml,
            category_slugs: TRAVIOOR.categorySlugs,
            weather: TRAVIOOR.weather,
            offTheRoadWeather: TRAVIOOR.offTheRoadWeather,
            locale: TRAVIOOR.locale
        }))
    })(app, TRAVIOOR);
    (function(app) {
        "use strict";
        app.constant("routes", Object.freeze({
            genericAjaxNavigationRoutes: ["/", "/_=_", "/articles/", "/articulos/", "/authors/", "/booking/recovery", "/careers/", "/careers/:role", "/colaboradores/", "/corporate", "/destinations/", "/destinations/:country", "/destinations/:country/:city", "/destinos/", "/destinos/:country", "/destinos/:country/:city", "/empleo/", "/empleo/:role", "/empresas", "/eventos/", "/events/", "/flight/search", "/hotel/search", "/hoteles/busqueda", "/info/about-us", "/info/contact", "/info/cookies-policy", "/info/newsletter", "/info/privacy-policy", "/info/terms-conditions", "/info/write-for-us", "/informacion/contacto", "/informacion/escribe-para-nosotros", "/informacion/politica-cookies", "/informacion/politica-privacidad", "/informacion/quienes-somos", "/informacion/suscribete", "/informacion/terminos-condiciones", "/login", "/profile", "/profile/edit-profile", "/register/", "/reservas/recuperacion", "/vuelos/busqueda"],
            noReloadOnSearchRoutes: ["/flight/results", "/hotel/results", "/hoteles/resultados", "/vuelos/resultados"],
            forcedScrollRoutes: ["/articles/:title", "/articulos/:title", "/destinations/:country/places/:otrd", "/destinos/:country/lugares/:otrd", "/eventos/:country/:city/:title", "/events/:country/:city/:title"]
        }))
    })(app);
    (function(app, TRAVIOOR) {
        "use strict";
        app.config(["$routeProvider", "$interpolateProvider", "$locationProvider", "$httpProvider", "tmhDynamicLocaleProvider", "routes", function($routeProvider, $interpolateProvider, $locationProvider, $httpProvider, tmhDynamicLocaleProvider, routes) {
            var idx, genericAjaxNavigationRoutes, noReloadOnSearchRoutes, forcedScrollRoutes;
            tmhDynamicLocaleProvider.localeLocationPattern("/bower_components/angular-i18n/angular-locale_<<locale>>.js");
            $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
            $locationProvider.html5Mode(true);
            genericAjaxNavigationRoutes = routes.genericAjaxNavigationRoutes;
            for (idx in genericAjaxNavigationRoutes) {
                $routeProvider.when(genericAjaxNavigationRoutes[idx], {
                    templateUrl: genericAjaxNavigationTemplateUrl
                })
            }
            noReloadOnSearchRoutes = routes.noReloadOnSearchRoutes;
            for (idx in noReloadOnSearchRoutes) {
                $routeProvider.when(noReloadOnSearchRoutes[idx], {
                    templateUrl: genericAjaxNavigationTemplateUrl,
                    reloadOnSearch: false
                })
            }
            forcedScrollRoutes = routes.forcedScrollRoutes;
            for (idx in forcedScrollRoutes) {
                $routeProvider.when(forcedScrollRoutes[idx], {
                    templateUrl: genericAjaxNavigationTemplateUrl,
                    resolve: {
                        injectVars: ["$route", function($route) {
                            $route.current.params.forceScrollTop = true
                        }]
                    }
                })
            }
            $routeProvider.otherwise({
                resolve: {
                    check: ["$location", "$rootScope", "$q", conditionalRedirect]
                }
            });

            function conditionalRedirect($location, $rootScope, $q) {
                var currentUrl, deferred, redirect = false;
                currentUrl = window.location.origin + window.location.pathname;
                if (currentUrl.slice(-1) !== "/") {
                    currentUrl += "/"
                }
                if (forceServersideRequest($rootScope, $location, $q)) {
                    redirect = true
                } else if (currentUrl !== TRAVIOOR.hostUrl) {
                    redirect = redirectNonExistingUrls()
                }
                if (redirect) {
                    deferred = $q.defer();
                    return deferred.promise
                }
            }

            function forceServersideRequest($rootScope, $location) {
                var targetUrl;
                if (true !== isHtml5NavigationReady($rootScope)) {
                    return false
                }
                targetUrl = angular.element("base").attr("href") + $location.url().substr(1);
                window.location.href = targetUrl;
                return true
            }

            function isHtml5NavigationReady($rootScope) {
                if (!$rootScope.html5NavigationReady) {
                    return false
                }
                return true
            }

            function redirectNonExistingUrls() {
                var $mainApp, head, targetUrl, metaHttpEquiv, linkCanonical;
                $mainApp = $("#main-app");
                if ($mainApp.hasClass("page-")) {
                    head = document.getElementsByTagName("head")[0];
                    targetUrl = angular.element("base").attr("href") + 404;
                    linkCanonical = document.createElement("link");
                    linkCanonical.rel = "canonical";
                    linkCanonical.href = targetUrl;
                    head.appendChild(linkCanonical);
                    metaHttpEquiv = document.createElement("meta");
                    metaHttpEquiv.httpEquiv = "refresh";
                    metaHttpEquiv.content = "0; url=" + targetUrl;
                    head.appendChild(metaHttpEquiv);
                    return true
                }
                return false
            }
            $interpolateProvider.startSymbol("<<").endSymbol(">>")
        }]);
        app.config(["$httpProvider", function($httpProvider) {
            $httpProvider.interceptors.push(httpInterceptors);
            httpInterceptors.$inject = ["$q", "$rootScope"];

            function httpInterceptors($q, $rootScope) {
                return {
                    requestError: errorHandler,
                    responseError: errorHandler
                };

                function errorHandler(rejection) {
                    var headers = {};
                    if (rejection.config && rejection.config.headers) {
                        headers = rejection.config.headers
                    }
                    if (headers["X-Requested-Context"] === "partial") {
                        return $q.reject(rejection)
                    }
                    if (rejection.data) {
                        $rootScope.resetTemplateCache = true;
                        return rejection
                    }
                    return $q.reject(rejection)
                }
            }
        }]);

        function genericAjaxNavigationTemplateUrl() {
            return window.location.href
        }
        app.config(["uibDatepickerConfig", function(uibDatepickerConfig) {
            var now = new Date,
                daysInYear = 365,
                oneDay = 864e5;
            now.setHours(12, 0, 0, 0);
            uibDatepickerConfig.formatYear = "yy";
            uibDatepickerConfig.dateFormat = "d MMM";
            uibDatepickerConfig.dateAsParameterFormat = "yyyy-MM-dd";
            uibDatepickerConfig.showWeeks = false;
            uibDatepickerConfig.formatDay = "d";
            uibDatepickerConfig.formatDayHeader = "EEE";
            uibDatepickerConfig.startingDay = 1;
            uibDatepickerConfig.minDate = new Date(now).getTime();
            uibDatepickerConfig.today = new Date(now.getTime() + oneDay).getTime();
            uibDatepickerConfig.nextWeek = new Date(now.getTime() + oneDay * 7).getTime();
            uibDatepickerConfig.inEightDays = new Date(now.getTime() + oneDay * 8).getTime();
            uibDatepickerConfig.maxDate = now;
            if (isLeapYear(uibDatepickerConfig.maxDate.getFullYear())) {
                daysInYear += 1
            }
            uibDatepickerConfig.maxDate = uibDatepickerConfig.maxDate.setDate(uibDatepickerConfig.maxDate.getDate() + daysInYear);

            function isLeapYear(year) {
                return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0 ? true : false
            }
        }]);
        app.config(["$compileProvider", function($compileProvider) {
            $compileProvider.debugInfoEnabled(TRAVIOOR.loggerLevel !== 0)
        }]);
        app.config(["$analyticsProvider", function($analyticsProvider) {
            var excludeAngularticsRoutes = ["/flight/results", "/vuelos/resultados", "/flight/booking/passengers", "/vuelos/reservas/pasajeros", "/booking/payment", "/reservas/pago", "/booking/confirmation", "/reservas/confirmacion"];
            $analyticsProvider.excludeRoutes(excludeAngularticsRoutes)
        }]);
        app.constant("_", window._).run(["$rootScope", function($rootScope) {
            return $rootScope._ = window._
        }])
    })(app, TRAVIOOR);
    (function(app) {
        "use strict";
        app.run(angularViewContentHandler);
        app.run(routeChangeBehaviour);
        app.run(templateCacheInterpolationFix);
        app.run(globalEventTracker);
        app.run(setLocale);
        routeChangeBehaviour.$inject = ["$templateCache", "$route", "$location", "$rootScope", "$anchorScroll", "$injector", "ngProgressFactory"];

        function routeChangeBehaviour($templateCache, $route, $location, $rootScope, $anchorScroll, $injector, ngProgressFactory) {
            var progressbar = ngProgressFactory.createInstance();
            $rootScope.$on("$routeChangeStart", routeChangeStartHandler);
            $rootScope.$on("$routeChangeSuccess", routeChangeSuccessHandler);
            $rootScope.$on("$routeChangeError", function() {
                angular.element("body").addClass("ready");
                angular.element("body").removeClass("loading-progress");
                progressbar.reset()
            });
            $rootScope.$on("$routeUpdate", function() {});
            $rootScope.$on("$viewContentLoaded", function() {
                setMainAppCssClasses();
                if ($location.hash() !== "") {
                    scrollToAnchor()
                }
            });

            function routeChangeStartHandler(event, next) {
                angular.element(".travioor-modal").modal("hide");
                angular.element(".modal-backdrop").hide();
                if (angular.element("body").hasClass("modal-open")) {
                    angular.element("body").removeClass("modal-open");
                    angular.element("body").removeAttr("style")
                }
                if ($rootScope.html5NavigationReady) {
                    angular.element("body").removeClass("ready");
                    angular.element("body").addClass("loading-progress");
                    progressbar.start()
                }
                if (!next.$$route) {
                    return
                }
                $rootScope.prevRoute = $rootScope.currentRoute;
                $rootScope.currentRoute = $rootScope.nextRoute;
                $rootScope.nextRoute = next
            }

            function routeChangeSuccessHandler(angularEvent, next, current) {
                if (next.$$route) {
                    cleanPageContent(next)
                }

                function comesFromRouteNotListed() {
                    return $rootScope.html5NavigationReady && !$rootScope.currentRoute
                }

                function hasChangedRoute() {
                    return $rootScope.currentRoute && $rootScope.currentRoute.$$route !== $rootScope.nextRoute.$$route
                }
                if (next && next.$$route) {
                    if (next.params.forceScrollTop || comesFromRouteNotListed() || hasChangedRoute()) {
                        scrollTop()
                    }
                }
                if (mustInvalidateCache(current)) {
                    if (next.templateUrl) {
                        $templateCache.remove(next.loadedTemplateUrl)
                    }
                    $rootScope.resetTemplateCache = false
                }
                notifyDocumentReady()
            }

            function mustInvalidateCache(currentRoute) {
                return $rootScope.resetTemplateCache === true || typeof currentRoute === "undefined"
            }

            function setMainAppCssClasses() {
                if (!$rootScope.html5NavigationReady) {
                    $rootScope.html5NavigationReady = true
                }
                angular.element("#main-app").attr("class", angular.element("#content-classes").data("class"))
            }

            function notifyDocumentReady() {
                $injector.get("$timeout")(function() {
                    angular.element("body").addClass("ready");
                    angular.element("body").removeClass("loading-progress");
                    progressbar.complete();
                    $rootScope.$broadcast("documentReady");
                    $rootScope.html5NavigationReady = true
                })
            }

            function cleanPageContent() {
                if ($route.current.templateUrl) {
                    $(".page-content").not("[data-ng-view]").remove()
                }
            }

            function scrollTop() {
                window.scrollTo(0, 0)
            }

            function scrollToAnchor() {
                $anchorScroll.yOffset = 80;
                $anchorScroll()
            }
        }
        angularViewContentHandler.$inject = ["$templateCache", "$route", "$location"];

        function angularViewContentHandler($templateCache, $route, $location) {
            var idx, route, viewContent;
            viewContent = $("div.angularViewContent");
            if (viewContent.length > 0 && viewContent.html() !== "") {
                for (idx in $route.routes) {
                    route = $route.routes[idx];
                    if (route.templateUrl && $location.path().match(route.regexp)) {
                        if (typeof route.templateUrl === "function") {
                            $templateCache.put(route.templateUrl(), viewContent.html())
                        } else {
                            $templateCache.put(route.templateUrl, viewContent.html())
                        }
                        break
                    }
                }
            }
        }
        templateCacheInterpolationFix.$inject = ["$templateCache", "$interpolate"];

        function templateCacheInterpolationFix($templateCache, $interpolate) {
            var i, fixedTemplate, templatesToFix = ["uib/template/datepicker/day.html", "uib/template/datepicker/month.html", "uib/template/datepicker/year.html", "uib/template/pagination/pagination.html"];
            for (i in templatesToFix) {
                if ($templateCache.get(templatesToFix[i])) {
                    fixedTemplate = $templateCache.get(templatesToFix[i]);
                    fixedTemplate = fixedTemplate.replace(/{{/g, $interpolate.startSymbol() + " ");
                    fixedTemplate = fixedTemplate.replace(/}}/g, " " + $interpolate.endSymbol());
                    $templateCache.remove(templatesToFix[i]);
                    $templateCache.put(templatesToFix[i], fixedTemplate)
                }
            }
        }
        globalEventTracker.$inject = ["$analytics"];

        function globalEventTracker($analytics) {
            $("body").on("click", "a.commercial-link", function() {
                var element = $(this);
                $analytics.eventTrack("Commercial link", {
                    category: "CTA",
                    label: element.text()
                })
            })
        }
        setLocale.$inject = ["consts", "tmhDynamicLocale"];

        function setLocale(consts, tmhDynamicLocale) {
            tmhDynamicLocale.set(consts.locale)
        }
    })(app);
    (function() {
        "use strict";
        angular.module("app").directive("bannerTracking", bannerTracking);

        function bannerTracking() {
            return {
                restrict: "A",
                controller: bannerTrackingController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {}
            }
        }
        bannerTrackingController.$inject = ["$element"];

        function bannerTrackingController($element) {
            angular.element($element).find("a").click(function(event) {
                var $link = angular.element(this),
                    href = $link.attr("href"),
                    target = $link.attr("target") || "_self";
                if (window.ga && window.open) {
                    event.stopPropagation();
                    event.preventDefault();
                    window.ga("send", "event", "banner", "click", href, {
                        transport: "beacon",
                        hitCallback: function() {
                            window.open(href, target)
                        }
                    })
                }
            })
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("skyscraperScroll", skyscraperScroll);

        function skyscraperScroll() {
            return {
                restrict: "A",
                controller: skyscraperScrollController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: true
            }
        }
        skyscraperScrollController.$inject = ["$scope"];

        function skyscraperScrollController($scope) {
            var dvm = this;

            function changeBannerOnScroll(event, yOffset) {
                dvm.scrolled = yOffset >= 370;
                if ($scope.$root.$$phase !== "$apply" && $scope.$root.$$phase !== "$digest") {
                    $scope.$apply()
                }
            }
            $scope.$on("scrolling", changeBannerOnScroll);
            dvm.scrolled = null
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("exitIntent", exitIntent);

        function exitIntent() {
            return {
                restrict: "A",
                controller: exitIntentController,
                controllerAs: "dvm",
                scope: {
                    force: "=?",
                    aggressive: "=?",
                    timer: "=?",
                    delay: "=?",
                    expires: "=?",
                    cookiename: "=?",
                    cookievalue: "=?",
                    subscriptionDone: "=?"
                },
                template: function(element) {
                    return element.html()
                },
                compile: function() {
                    return {
                        pre: function(scope) {
                            scope.setInitialParameters()
                        },
                        post: function() {}
                    }
                }
            }
        }
        exitIntentController.$inject = ["$element", "$rootScope", "$scope", "$cookies", "$timeout", "$analytics"];

        function exitIntentController($element, $rootScope, $scope, $cookies, $timeout, $analytics) {
            var dvm = this;
            dvm.currentStep = 1;
            dvm.sendFormSuccess = sendFormSuccess;

            function init() {
                if (dvm.force) {
                    $timeout(notifyEvent)
                } else if (dvm.aggressive || notTriggeredYet()) {
                    bindMouseleave()
                }
            }

            function bindMouseleave() {
                $(document).mouseleave(mouseleaveHandler)
            }

            function unbindMouseleave() {
                $(document).off("mouseleave", mouseleaveHandler)
            }

            function mouseleaveHandler(event) {
                if (event.clientY <= 0) {
                    $timeout.cancel(dvm.delayedTask);
                    dvm.delayedTask = $timeout(notifyEvent, dvm.delay)
                }
            }

            function notifyEvent() {
                $cookies.put(dvm.cookiename, dvm.cookievalue, {
                    expires: dvm.expires
                });
                unbindMouseleave();
                $rootScope.$broadcast("exitIntent");
                if ($element.hasClass("modal")) {
                    $element.modal("show");
                    $analytics.eventTrack("Trigger", {
                        category: "Subscription",
                        label: "Popup",
                        nonInteraction: true
                    })
                }
            }

            function sendFormSuccess() {
                dvm.currentStep = 2;
                $analytics.eventTrack("Success", {
                    category: "Subscription",
                    label: "Popup"
                })
            }

            function notTriggeredYet() {
                return $cookies.get(dvm.cookiename) !== dvm.cookievalue
            }

            function setSubscriptionDoneCookie() {
                var expiresAfterSubscription = "Tue, 19 Jan 2038 03:14:07 UTC",
                    cookieNameAfterSubscription = "signedToNewsletter",
                    cookieValueAfterSubscription = "true";
                $cookies.put(cookieNameAfterSubscription, cookieValueAfterSubscription, {
                    expires: expiresAfterSubscription
                })
            }
            $scope.setInitialParameters = function() {
                var expirationDate = new Date;
                expirationDate.setDate(expirationDate.getDate() + 3);
                dvm.force = $scope.force || false;
                dvm.aggressive = $scope.aggressive || false;
                dvm.delay = $scope.delay || 0;
                dvm.expires = $scope.expires || expirationDate.toUTCString();
                dvm.cookiename = $scope.cookiename || "firstRunTriggered";
                dvm.cookievalue = $scope.cookievalue || "true";
                if ($scope.subscriptionDone && $element.hasClass("modal")) {
                    dvm.currentStep = 3;
                    $timeout(function() {
                        $element.modal("show")
                    });
                    setSubscriptionDoneCookie();
                    return
                }
                init()
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("genericAjaxForm", genericAjaxForm);

        function genericAjaxForm() {
            return {
                restrict: "A",
                controller: genericAjaxFormController
            }
        }
        genericAjaxFormController.$inject = ["$element", "$scope", "$location"];

        function genericAjaxFormController($element, $scope, $location) {
            $element.find("form").on("submit", function(event) {
                var uriArguments, name;
                uriArguments = $location.search();
                event.preventDefault();
                event.stopPropagation();
                $(this).find("input,select").each(function() {
                    name = $(this).attr("name");
                    uriArguments[name] = $(this).val()
                });
                $location.search(uriArguments);
                $location.replace();
                if ($scope.$root.$$phase !== "$apply" && $scope.$root.$$phase !== "$digest") {
                    $scope.$apply()
                }
            })
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("genericHeaderSearchForm", genericHeaderSearchForm);

        function genericHeaderSearchForm() {
            return {
                restrict: "A",
                controller: genericHeaderSearchFormController,
                controllerAs: "dvm",
                scope: {},
                template: function(element) {
                    return element.html()
                },
                compile: function() {
                    return {
                        pre: function() {},
                        post: function() {}
                    }
                }
            }
        }
        genericHeaderSearchFormController.$inject = ["$element", "$timeout"];

        function genericHeaderSearchFormController($element, $timeout) {
            var dvm = this,
                inputSelector = "input[type=text]";

            function activate() {
                dvm.searchActive = true;
                $timeout(function() {
                    $element.find(inputSelector).focus()
                }, 600)
            }

            function deactivate() {
                dvm.searchQuery = "";
                dvm.searchActive = false;
                $timeout(function() {
                    $element.find("#search-button-traditional").click()
                })
            }
            dvm.searchQuery = $element.find(inputSelector).val();
            dvm.searchActive = $element.find(inputSelector).val() !== "";
            dvm.activate = activate;
            dvm.deactivate = deactivate
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("newsletterForm", newsletterForm);

        function newsletterForm() {
            return {
                restrict: "A",
                controller: newsletterFormController,
                controllerAs: "dvm",
                scope: {
                    afterSend: "&?"
                },
                template: function(element) {
                    return element.html()
                }
            }
        }
        newsletterFormController.$inject = ["$scope", "$element", "$http", "$analytics"];

        function newsletterFormController($scope, $element, $http, $analytics) {
            var dvm = this;
            dvm.success = false;
            dvm.first_name = "";
            dvm.last_name = "";
            dvm.email = "";
            dvm.sendForm = sendForm;
            dvm.sendFormError = false;
            dvm.invalidFormError = false;
            dvm.sending = false;
            dvm.setFieldClass = setFieldClass;

            function sendForm(action) {
                var formData;
                if (!validateForm()) {
                    return
                }
                $element.addClass("loading");
                dvm.sending = true;
                dvm.invalidFormError = false;
                formData = {
                    first_name: dvm.first_name,
                    last_name: dvm.last_name,
                    email: dvm.email
                };
                $http({
                    method: "POST",
                    url: action,
                    data: $.param(formData),
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                        "X-Requested-Context": "partial"
                    }
                }).then(sendFormSuccess, sendFormError).finally(removeLoading)
            }

            function sendFormSuccess(response) {
                if (checkResponse(response.data)) {
                    subscriptionSuccess()
                } else {
                    subscriptionFail()
                }
            }

            function checkResponse(responseData) {
                if (responseData === true || responseData.code === 214 || responseData.code === -99) {
                    return true
                }
                return false
            }

            function sendFormError() {
                dvm.sendFormError = true
            }

            function subscriptionSuccess() {
                dvm.success = true;
                dvm.sendFormError = false;
                if (_.isFunction($scope.afterSend)) {
                    $scope.afterSend()
                } else {
                    $analytics.eventTrack("Success", {
                        category: "Subscription",
                        label: "Form"
                    })
                }
            }

            function subscriptionFail() {
                dvm.sendFormError = true
            }

            function removeLoading() {
                $element.removeClass("loading");
                dvm.sending = false
            }

            function setFieldClass(field) {
                var classes = [],
                    errorClass = "error",
                    touchedClass = "touched";
                if ($scope.newsletterForm[field].$invalid && classes.indexOf(errorClass) === -1) {
                    classes.push(errorClass)
                }
                if ($scope.newsletterForm[field].$touched && classes.indexOf(touchedClass) === -1) {
                    classes.push(touchedClass)
                }
                return classes.join(" ")
            }

            function validateForm() {
                var newsletterForm = $scope.newsletterForm,
                    valid = newsletterForm.$valid;
                if (!valid) {
                    angular.forEach(newsletterForm.$error, function(field) {
                        angular.forEach(field, function(errorField) {
                            errorField.$setTouched();
                            setFieldClass(errorField.$name)
                        })
                    });
                    dvm.invalidFormError = true
                }
                return valid
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("waitUntilAngularViewReady", waitUntilAngularViewReady);
        waitUntilAngularViewReady.$inject = ["$compile"];

        function waitUntilAngularViewReady($compile) {
            return {
                restrict: "A",
                replace: false,
                terminal: true,
                priority: 1e3,
                link: function link(scope, element) {
                    var cleanUpFunc, applied = false;
                    if (element.parents("[data-ng-view]").length > 0) {
                        element.removeAttr("data-wait-until-angular-view-ready");
                        $compile(element)(scope)
                    } else {
                        cleanUpFunc = scope.$on("documentReady", function() {
                            if (!applied) {
                                element.removeAttr("data-wait-until-angular-view-ready");
                                $compile(element)(scope)
                            }
                            applied = true
                        });
                        element.on("$destroy", function() {
                            cleanUpFunc()
                        })
                    }
                }
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("footerFeed", footerFeed);

        function footerFeed() {
            return {
                restrict: "A",
                controller: footerFeedController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {}
            }
        }
        footerFeedController.$inject = [];

        function footerFeedController() {
            var dvm = this,
                $feeds = $(".feeds");
            dvm.toggle = toggle;

            function toggle($event) {
                $feeds.slideToggle();
                $($event.target).toggleClass("active")
            }

            function init() {
                $feeds.hide()
            }
            init()
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("footerNavAccordion", footerNavAccordion);

        function footerNavAccordion() {
            return {
                restrict: "A",
                controller: footerNavAccordionController,
                controllerAs: "footerNavToggle",
                template: function(element) {
                    return element.html()
                },
                scope: true
            }
        }
        footerNavAccordionController.$inject = [];

        function footerNavAccordionController() {
            var dvm = this;
            dvm.onClick = onClick;

            function onClick($event) {
                var p = angular.element($event.currentTarget),
                    container = p.parent(),
                    links = p.next(),
                    className = "close-links";
                if (container.hasClass(className)) {
                    container.removeClass(className);
                    links.slideDown()
                } else {
                    container.addClass(className);
                    links.slideUp()
                }
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("headerScroll", headerScroll);

        function headerScroll() {
            return {
                restrict: "A",
                controller: headerScrollController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: true
            }
        }
        headerScrollController.$inject = ["$rootScope", "$scope", "screenSize"];

        function headerScrollController($rootScope, $scope, screenSize) {
            var dvm = this,
                $body = $("html, body"),
                mobileScreenSize = "xs";

            function changeHeaderOnScroll(event, yOffset) {
                dvm.scrolled = yOffset >= 80 || screenSize.is(mobileScreenSize);
                if ($scope.$root.$$phase !== "$apply" && $scope.$root.$$phase !== "$digest") {
                    $scope.$apply()
                }
            }

            function toggleMenu() {
                scrollToTop();
                dvm.menuDown = !dvm.menuDown;
                $rootScope.$broadcast("headerMenuDown", dvm.menuDown);
                angular.element(".travioor-modal.in").modal("hide");
                angular.element(".modal-backdrop").hide();
                if (angular.element("body").hasClass("modal-open")) {
                    angular.element("body").removeClass("modal-open");
                    angular.element("body").removeAttr("style")
                }
            }

            function scrollToTop() {
                $body.animate({
                    scrollTop: 0
                }, 200)
            }
            $scope.$on("scrolling", changeHeaderOnScroll);
            dvm.scrolled = screenSize.is(mobileScreenSize);
            dvm.menuDown = false;
            $scope.$on("headerMenuDown", function(event, value) {
                dvm.menuDown = value
            });
            dvm.toggleMenu = toggleMenu
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("isRead", isRead);

        function isRead() {
            return {
                restrict: "A",
                controller: isReadController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    readItem: "@"
                }
            }
        }
        isReadController.$inject = ["$element", "$scope", "$window", "$analytics"];

        function isReadController($element, $scope, $window, $analytics) {
            var dvm = this;

            function init() {
                dvm.readItem = $scope.readItem || "item";
                dvm.reached = false;
                dvm.halfReached = false;
                dvm.pageYOffset = 0;
                $scope.$on("scrolling", onScroll)
            }

            function onScroll(event, $pageYOffset) {
                dvm.pageYOffset = $pageYOffset;
                if (dvm.reached === false) {
                    dvm.reached = check()
                }
            }

            function check() {
                var reached = reach(1),
                    halfReached = reach(2);
                if (halfReached && !dvm.halfReached) {
                    track("half");
                    dvm.halfReached = true
                }
                if (reached) {
                    track("end")
                }
                return reached
            }

            function reach(divisor) {
                var bottomLimit = $element.height() + $element.offset().top;
                return bottomLimit > 0 && $window.innerHeight > bottomLimit / divisor - dvm.pageYOffset
            }

            function track(val) {
                $analytics.eventTrack("Read", {
                    category: val,
                    label: "url",
                    value: dvm.readItem
                })
            }
            init()
        }
    })();
    (function($) {
        "use strict";
        angular.module("app").directive("loadMore", loadMore);

        function loadMore() {
            return {
                restrict: "EA",
                scope: {
                    url: "=",
                    appendToSelector: "=",
                    firstPage: "=",
                    limitRequest: "=?",
                    totalItems: "=?"
                },
                controller: loadMoreController,
                controllerAs: "vm",
                template: function(element) {
                    return element.html()
                }
            }
        }
        loadMoreController.$inject = ["$scope", "$http", "$analytics"];

        function loadMoreController($scope, $http, $analytics) {
            var vm = this,
                showLoader = "show-loader",
                $containerAppend = $($scope.appendToSelector),
                nextPage = $scope.firstPage;

            function checkMorePages(html) {
                if ($scope.limitRequest && $scope.totalItems) {
                    return $scope.limitRequest * nextPage >= $scope.totalItems
                }
                return !$.trim(html)
            }

            function processResult(html) {
                $($.parseHTML(html)).appendTo($containerAppend).hide().addClass("tmp");
                $containerAppend.find(".tmp").each(function() {
                    $(this).fadeIn().removeClass("tmp")
                })
            }

            function load($event) {
                var $loadMore = angular.element($event.currentTarget),
                    nextPageUrl = $scope.url + nextPage;
                $event.preventDefault();
                vm.error = false;
                $loadMore.prop("disabled", true).addClass(showLoader);
                $http.get(nextPageUrl, {
                    headers: {
                        "X-Requested-Target": "pagination",
                        "X-Requested-Context": "partial"
                    }
                }).then(function(result) {
                    var html = result.data;
                    processResult(html);
                    if (checkMorePages(html)) {
                        $loadMore.parent().hide()
                    }
                    $analytics.pageTrack(nextPageUrl.replace("xhr/", ""));
                    nextPage++
                }).catch(function() {
                    vm.error = true
                }).finally(function() {
                    $loadMore.prop("disabled", false).removeClass(showLoader)
                })
            }
            vm.error = false;
            vm.load = load
        }
    })(jQuery);
    (function() {
        "use strict";
        angular.module("app").directive("pageScroll", pageScroll);

        function pageScroll() {
            return {
                restrict: "A",
                controller: pageScrollController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: false,
                compile: function() {
                    return {
                        post: function(scope) {
                            scope.init()
                        }
                    }
                }
            }
        }
        pageScrollController.$inject = ["$rootScope", "$scope", "$window", "$element", "$timeout"];

        function pageScrollController($rootScope, $scope, $window, $element, $timeout) {
            var onScrollThrottled;

            function onScroll() {
                $timeout(broadcast)
            }

            function broadcast() {
                $rootScope.$broadcast("scrolling", $window.pageYOffset)
            }
            onScrollThrottled = _.throttle(onScroll, 10, {
                leading: true
            });
            angular.element($window).bind("scroll", onScrollThrottled);
            $element.on("$destroy", function() {
                angular.element($window).unbind("scroll", onScrollThrottled)
            });
            $scope.init = function() {
                onScroll()
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("refreshPageMetadata", refreshPageMetadataDirective);

        function refreshPageMetadataDirective() {
            return {
                restrict: "EA",
                link: function(scope, element, attrs) {
                    var title, description, image;
                    title = attrs.title;
                    description = attrs.description;
                    image = attrs.image;
                    if (title) {
                        refreshTitle(title)
                    }
                    if (description) {
                        refreshDescription(description)
                    }
                    if (image) {
                        refreshImage(image)
                    }
                }
            }
        }

        function refreshTitle(title) {
            angular.element("title").html(title);
            angular.element('meta[property="og:title"]').attr("content", title);
            angular.element('meta[name="twitter:title"]').attr("content", title)
        }

        function refreshDescription(description) {
            angular.element("meta[name=description]").attr("content", description);
            angular.element('meta[property="og:description"]').attr("content", description);
            angular.element('meta[name="twitter:description"]').attr("content", description)
        }

        function refreshImage(image) {
            angular.element('meta[property="og:image"]').attr("content", image);
            angular.element('meta[name="twitter:image"]').attr("content", image)
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("secondNavScroll", secondNavScroll);

        function secondNavScroll() {
            return {
                restrict: "A",
                controller: secondNavScrollController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: true
            }
        }
        secondNavScrollController.$inject = ["$scope"];

        function secondNavScrollController($scope) {
            var dvm = this,
                $body = $("html, body");

            function changeNavOnScroll(event, yOffset) {
                dvm.scrolled = yOffset >= 370;
                if ($scope.$root.$$phase !== "$apply" && $scope.$root.$$phase !== "$digest") {
                    $scope.$apply()
                }
            }

            function scrollTo(event) {
                var selector = event.target.hash,
                    offset;
                event.preventDefault();
                offset = dvm.scrolled ? 60 : 100;
                $body.animate({
                    scrollTop: angular.element(selector).offset().top - offset
                }, 200)
            }
            $scope.$on("scrolling", changeNavOnScroll);
            dvm.scrolled = null;
            dvm.scrollTo = scrollTo
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("selectOnClick", selectOnClick);
        selectOnClick.$inject = ["$window"];

        function selectOnClick($window) {
            var directive = {
                restrict: "A",
                scope: {},
                link: linkFunc
            };
            return directive;

            function linkFunc(scope, element) {
                element.on("click", function() {
                    if (!$window.getSelection().toString()) {
                        this.setSelectionRange(0, this.value.length)
                    }
                })
            }
        }
    })();
    (function($) {
        "use strict";
        angular.module("app").directive("socialShare", socialShare);

        function socialShare() {
            return {
                restrict: "A",
                controller: socialShareController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    url: "@"
                }
            }
        }
        socialShareController.$inject = ["$element", "$scope", "$analytics"];

        function socialShareController($element, $scope, $analytics) {
            var $shareButtons = angular.element($element).find("a");

            function unbindClick() {
                $shareButtons.unbind("click", handleClick)
            }

            function handleClick() {
                var $link = angular.element(this);
                $analytics.eventTrack("Social Share", {
                    category: $.trim($link.text()),
                    label: $scope.url
                })
            }
            $shareButtons.click(handleClick);
            $element.on("$destroy", unbindClick)
        }
    })(jQuery);
    (function() {
        "use strict";
        angular.module("app").directive("socialShareToggle", socialShareToggle);

        function socialShareToggle() {
            return {
                restrict: "A",
                controller: socialShareToggleController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {}
            }
        }
        socialShareToggleController.$inject = ["$scope", "$timeout", "$element"];

        function socialShareToggleController($scope, $timeout, $element) {
            var dvm = this;
            dvm.toggle = toggle;

            function toggle(event, className) {
                $("#social-share").slideToggle(400, function() {
                    $(this).toggleClass(className).removeAttr("style")
                });
                $element.find(".social-share-button span").toggleClass("hide")
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("spinnerOnSubmission", spinnerOnSubmission);

        function spinnerOnSubmission() {
            return {
                restrict: "A",
                controller: spinnerOnSubmissionController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {}
            }
        }
        spinnerOnSubmissionController.$inject = ["$scope", "$element"];

        function spinnerOnSubmissionController($scope, $element) {
            var dvm = this;

            function handleOnSubmission() {
                $scope.$apply(function() {
                    dvm.sending = true;
                    $element.find("button[type=submit]").attr("disabled", true)
                })
            }
            $element.on("$destroy", function() {
                $element.unbind("submit", handleOnSubmission)
            });
            $element.on("submit", handleOnSubmission);
            dvm.sending = false
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("standardPagination", standardPagination);

        function standardPagination() {
            return {
                restrict: "A",
                controller: standardPaginationController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    elementsSelector: "@",
                    totalItems: "=",
                    nElementsPage: "=",
                    onChangeEvent: "@?"
                }
            }
        }
        standardPaginationController.$inject = ["$scope", "$rootScope"];

        function standardPaginationController($scope, $rootScope) {
            var dvm = this,
                elements = angular.element($scope.elementsSelector),
                $body = $("html, body");
            dvm.totalItems = $scope.totalItems;
            dvm.initialTotalItems = dvm.totalItems;
            dvm.nElementsPage = $scope.nElementsPage;
            dvm.currentPage = 1;
            dvm.numPages = Math.ceil(dvm.totalItems / dvm.nElementsPage);
            dvm.onChangeEvent = $scope.onChangeEvent;
            dvm.pageChange = pageChange;
            $rootScope.$on("resultsFiltered", resultsListener);
            setVisibles();

            function pageChange() {
                setVisibles(true)
            }

            function setVisibles(shouldAnimate) {
                var start = (dvm.currentPage - 1) * dvm.nElementsPage,
                    end = start + dvm.nElementsPage;
                elements.hide().slice(start, end).show();
                if (shouldAnimate) {
                    $body.animate({
                        scrollTop: angular.element("body").offset().top
                    }, 200)
                }
                if (dvm.onChangeEvent) {
                    $scope.$emit(dvm.onChangeEvent, {
                        nElementsPage: dvm.nElementsPage,
                        currentPage: dvm.currentPage
                    })
                }
            }

            function resultsListener() {
                elements = angular.element($scope.elementsSelector);
                dvm.totalItems = elements.length;
                dvm.currentPage = 1;
                dvm.numPages = Math.ceil(dvm.totalItems / dvm.nElementsPage);
                setVisibles(false)
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").service("CurrencyService", CurrencyService);
        CurrencyService.$inject = ["Utils"];

        function CurrencyService(Utils) {
            var currency;
            this.getCurrency = getCurrency;
            this.getCurrencyCode = getCurrencyCode;

            function getCurrency() {
                if (currency) {
                    return currency
                }
                try {
                    currency = JSON.parse(decodeURIComponent(Utils.getCookie("currency")))
                } catch (error) {
                    l.err(error);
                    currency = {}
                }
                return currency
            }

            function getCurrencyCode() {
                return getCurrency().code
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").service("PromiseQueueChain", PromiseQueueChain);
        PromiseQueueChain.$inject = ["$q", "PromiseQueue"];

        function PromiseQueueChain($q, PromiseQueue) {
            PromiseQueue.prototype.running = false;
            PromiseQueue.prototype.run = function(func, instant) {
                this.add(func, instant);
                if (!this.running) {
                    this.start()
                }
                return this
            };
            PromiseQueue.prototype.next = function() {
                var self = this,
                    func;
                if (self._pause === true) {
                    return
                }
                if (self._queue.length === 0) {
                    this.running = false;
                    return
                }
                this.running = true;
                func = self._queue.shift();
                promisify(func).then(function() {
                    self.next()
                })
            };

            function promisify(func) {
                var deferred = $q.defer();
                func(function() {
                    deferred.resolve()
                });
                return deferred.promise
            }
            return new PromiseQueue
        }
    })();
    (function(app) {
        "use strict";
        app.factory("Utils", function() {
            return {
                getCookie: getCookie,
                setCookie: setCookie,
                transformForSearch: transformForSearch,
                calculateAge: calculateAge
            };

            function getCookie(cname) {
                var i, c, name = cname + "=",
                    ca = document.cookie.split(";");
                for (i = 0; i < ca.length; i++) {
                    c = ca[i];
                    while (c.charAt(0) === " ") {
                        c = c.substring(1)
                    }
                    if (c.indexOf(name) === 0) {
                        return decodeURI(c.substring(name.length, c.length))
                    }
                }
                return false
            }

            function setCookie(name) {
                var date, years = 2;
                date = new Date;
                date.setTime(date.getTime() + years * 365 * 24 * 60 * 60 * 1e3);
                document.cookie = name + "=1; expires=" + date.toUTCString() + "; path=/";
                angular.element("#cookie-msg").remove()
            }

            function transformForSearch(str) {
                if (_.isString(str)) {
                    return removeAccents(str.toLowerCase())
                } else {
                    return ""
                }
            }

            function removeAccents(str) {
                if (!str.normalize) {
                    return str
                }
                return str.normalize("NFKD").replace(/[\u0300-\u036F]/g, "")
            }

            function calculateAge(dateOfBirth, dateToCalculate) {
                var calculateYear, calculateMonth, calculateDay, birthYear, birthMonth, birthDay, age, ageMonth, ageDay;
                calculateYear = dateToCalculate.getFullYear();
                calculateMonth = dateToCalculate.getMonth();
                calculateDay = dateToCalculate.getDate();
                birthYear = dateOfBirth.getFullYear();
                birthMonth = dateOfBirth.getMonth();
                birthDay = dateOfBirth.getDate();
                age = calculateYear - birthYear;
                ageMonth = calculateMonth - birthMonth;
                ageDay = calculateDay - birthDay;
                if (ageMonth < 0 || ageMonth === 0 && ageDay < 0) {
                    age = parseInt(age) - 1
                }
                return age
            }
        })
    })(app);
    (function() {
        "use strict";
        angular.module("app").controller("MainController", MainController);
        MainController.$inject = ["$scope", "$rootScope", "Utils"];

        function MainController($scope, $rootScope, Utils) {
            l.deb(60, "MainController loading");
            $scope.pageReady = false;
            $scope.menuDown = false;
            $scope.$on("headerMenuDown", function(event, value) {
                $scope.menuDown = value
            });
            $scope.$on("documentReady", function() {
                if (!$scope.pageReady) {
                    angular.element("body").addClass("documentReady")
                }
                $scope.pageReady = true
            });
            _.extend($scope, {
                setCookie: function() {
                    Utils.setCookie("acceptsCookies");
                    angular.element("#cookie-msg").remove()
                }
            });
            $scope.$on("$routeChangeSuccess", function() {
                $rootScope.$broadcast("headerMenuDown", false);
                $scope.pageReady = false;
                angular.element("#navbar-top-mobile").collapse("hide")
            })
        }
    })();
    (function() {
        "use strict";
        angular.module("app").factory("CategoryService", CategoryService);
        CategoryService.$inject = ["$http", "$q", "consts"];

        function CategoryService($http, $q, consts) {
            function Category() {
                var self = this,
                    deferred;
                this.categories = null;
                this.get = function() {
                    l.deb(50, "get Tags requested");
                    if (this.categories !== null) {
                        l.deb(50, "serving cached Tags");
                        deferred = $q.defer();
                        deferred.resolve(this.categories);
                        return deferred.promise
                    }
                    return $http.get(consts.category_slugs, {
                        cache: true,
                        headers: {
                            "X-Requested-Context": "partial"
                        }
                    }).then(function successCallback(response) {
                        l.deb(50, "get categories success");
                        self.categories = response.data;
                        return self.categories
                    }, function errorCallback(response) {
                        l.deb(50, "get categories failed", response);
                        return {}
                    })
                }
            }
            return new Category
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("authorOverview", authorOverview);

        function authorOverview() {
            return {
                restrict: "A",
                controller: authorOverviewController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    itemsUrl: "=",
                    itemsContainer: "="
                }
            }
        }
        authorOverviewController.$inject = ["$scope", "$rootScope", "$element", "$timeout", "$http", "Utils"];

        function authorOverviewController($scope, $rootScope, $element, $timeout, $http, Utils) {
            var dvm = this,
                selectedContent, timeout = 300,
                expandedContent = ".expanded-content",
                item = ".item-article",
                contents = ["description", "articles"],
                itemsUrl = $scope.itemsUrl,
                $itemsContainer = $element.find($scope.itemsContainer),
                initialItemsNum = 3,
                name, description;
            dvm.opened = !!$scope.opened;
            dvm.contentOpen = [];
            dvm.showSeeAll = false;
            dvm.error = false;
            dvm.itemsRetrieved = false;
            dvm.close = close;
            dvm.open = open;
            $rootScope.$on("filterAuthor", filterAuthor);

            function open(content) {
                if (dvm.opened && selectedContent === content) {
                    setClose();
                    return
                }
                dvm.opened ? setContent(content) : setOpen(content);
                if (dvm.itemsRetrieved) {
                    $timeout(function() {
                        showItems(0, initialItemsNum)
                    }, timeout)
                } else {
                    getItems()
                }
                selectedContent = content
            }

            function close() {
                setClose()
            }

            function setOpen(content) {
                dvm.opened = true;
                setContent(content);
                $element.find(expandedContent).slideDown(timeout);
                setShowSeeAll()
            }

            function setClose() {
                $element.find(expandedContent).slideUp(timeout);
                $timeout(function() {
                    dvm.opened = false;
                    setContent(false);
                    hideItems(getItemsNumber())
                }, timeout)
            }

            function setContent(content) {
                var c;
                for (c = 0; c < contents.length; c++) {
                    dvm.contentOpen[contents[c]] = contents[c] === content ? true : false
                }
            }

            function getItems() {
                $http.get(itemsUrl).then(function(result) {
                    processResult(result.data);
                    showItems(0, initialItemsNum)
                }).catch(function() {
                    dvm.error = true
                }).finally(function() {
                    dvm.itemsRetrieved = true
                })
            }

            function processResult(html) {
                $($.parseHTML(html)).appendTo($itemsContainer).hide();
                setShowSeeAll()
            }

            function showItems(n, m) {
                $itemsContainer.find(item).slice(n, m).fadeIn()
            }

            function hideItems(n) {
                $itemsContainer.find(item).slice(0, n).hide()
            }

            function getItemsNumber() {
                return $itemsContainer.find(item).length
            }

            function setShowSeeAll() {
                if (getItemsNumber() > initialItemsNum) {
                    dvm.showSeeAll = true
                }
            }

            function filterAuthor(event, data) {
                var searchToken = Utils.transformForSearch(data);
                if (!searchToken || matchesSearch(searchToken)) {
                    $element.removeClass("hidden")
                } else {
                    $element.addClass("hidden")
                }
                $scope.$emit("resultsFiltered")
            }

            function matchesSearch(searchToken) {
                if (!name) {
                    name = Utils.transformForSearch($element.find(".name").text())
                }
                if (!description) {
                    description = Utils.transformForSearch($element.find(".description").text())
                }
                return name.indexOf(searchToken) > -1 || description.indexOf(searchToken) > -1
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("authorSearch", authorSearch);

        function authorSearch() {
            return {
                restrict: "A",
                controller: authorSearchController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: true
            }
        }
        authorSearchController.$inject = ["$scope"];

        function authorSearchController($scope) {
            var dvm = this;
            dvm.filterQuery = "";
            dvm.onSubmit = onSubmit;
            dvm.inputChange = inputChange;
            dvm.closeSearch = closeSearch;

            function onSubmit($event) {
                $event.preventDefault();
                $event.stopPropagation()
            }

            function inputChange() {
                $scope.$emit("filterAuthor", dvm.filterQuery)
            }

            function closeSearch() {
                dvm.filterQuery = "";
                inputChange()
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("categoryFilter", categoryFilter);

        function categoryFilter() {
            return {
                restrict: "EA",
                controller: categoryFilterController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {},
                compile: function() {
                    return {
                        pre: function() {},
                        post: function() {}
                    }
                }
            }
        }
        categoryFilterController.$inject = ["$rootScope", "CategoryService", "$location"];

        function categoryFilterController($rootScope, CategoryService, $location) {
            var queryString, dvm = this;
            queryString = $location.search();

            function init() {
                CategoryService.get().then(function(categories) {
                    dvm.categories = categories
                })
            }

            function setCategory(slug) {
                if (dvm.selectedCategory !== slug) {
                    dvm.selectedCategory = slug;
                    $rootScope.$broadcast("events-filter-category-change", slug)
                }
            }
            dvm.categories = {};
            dvm.selectedCategory = queryString.category;
            dvm.setCategory = setCategory;
            init()
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("eventCalendar", eventCalendar);

        function eventCalendar() {
            return {
                restrict: "EA",
                scope: {},
                controller: eventCalendarController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                }
            }
        }
        eventCalendarController.$inject = ["$http", "consts", "$scope", "CategoryService", "$location", "$timeout"];

        function eventCalendarController($http, consts, $scope, CategoryService, $location, $timeout) {
            var queryString, dvm = this;
            queryString = $location.search();

            function autocompleteDestination(value) {
                return $http.get(consts.destinations_autocomplete, {
                    params: {
                        destination: value
                    },
                    headers: {
                        "X-Requested-Context": "partial"
                    }
                }).then(function(response) {
                    return response.data
                })
            }

            function reverseAutocompleteDestination(destinationId) {
                return $http.get(consts.destinations_autocomplete, {
                    params: {
                        id: destinationId
                    },
                    headers: {
                        "X-Requested-Context": "partial"
                    }
                }).then(function(response) {
                    var item;
                    if (response && response.data && response.data.length > 0) {
                        item = response.data[0];
                        dvm.destination = {
                            id: item.id,
                            name: item.name
                        }
                    }
                })
            }

            function setMonth(partialDate, month) {
                dvm.selectedMonth = month;
                dvm.selectedPartialDate = partialDate
            }

            function submit() {
                $location.search({
                    month: dvm.selectedPartialDate,
                    destination: dvm.destination ? dvm.destination.id : null,
                    category: dvm.selectedCategory
                })
            }

            function init() {
                if (dvm.selectedPartialDate) {
                    $timeout(function() {
                        angular.element("#events-search-form .month-selected").click()
                    })
                }
                if (queryString.destination) {
                    reverseAutocompleteDestination(queryString.destination)
                }
            }
            $scope.$on("events-filter-category-change", function($event, slug) {
                if (dvm.selectedCategory !== slug) {
                    dvm.selectedCategory = slug;
                    submit()
                }
            });
            $scope.$on("all-destinations-filter", function() {
                dvm.destination = {
                    id: null,
                    name: null
                }
            });
            dvm.category = queryString.category;
            dvm.destination = {
                id: queryString.destination,
                name: null
            };
            dvm.selectedMonth = null;
            dvm.selectedPartialDate = queryString.month;
            dvm.selectedCategory = queryString.category;
            dvm.autocompleteDestination = autocompleteDestination;
            dvm.setMonth = setMonth;
            dvm.submit = submit;
            init()
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("eventList", eventList);

        function eventList() {
            return {
                restrict: "A",
                controller: eventListController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    selectedMonth: "="
                }
            }
        }
        eventListController.$inject = ["$scope", "$compile", "EventsService"];

        function eventListController($scope, $compile, EventsService) {
            var dvm = this,
                $eventList = angular.element(".homepage-event-list");

            function monthChange(monthKey) {
                dvm.month = monthKey;
                getEventsAsHtml()
            }

            function getEventsAsHtml() {
                EventsService.getEventsByMonthAsHtml(dvm.month).then(function(events) {
                    $eventList.html(events.data);
                    $compile($eventList.contents())($scope)
                }).catch(function() {
                    $eventList.html("")
                })
            }
            dvm.monthChange = monthChange;
            dvm.month = $scope.selectedMonth || null;
            dvm.showAllEvents = false
        }
    })();
    (function() {
        "use strict";
        angular.module("app").service("EventsService", EventsService);
        EventsService.$inject = ["$http", "consts"];

        function EventsService($http, consts) {
            this.getEventsByMonthAsHtml = getEventsByMonthAsHtml;

            function getEventsByMonthUrl(month) {
                var url = consts.get_events_as_html;
                if (month) {
                    url += "/" + month
                }
                return url
            }

            function getEventsByMonthAsHtml(month) {
                var url = getEventsByMonthUrl(month);
                return $http.get(url, {
                    cache: true,
                    headers: {
                        "X-Requested-Context": "partial"
                    }
                })
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("bestWay", bestWay);

        function bestWay() {
            return {
                restrict: "A",
                controller: bestWayController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    opened: "="
                }
            }
        }
        bestWayController.$inject = ["$rootScope", "$scope", "screenSize", "$element"];

        function bestWayController($rootScope, $scope, screenSize, $element) {
            var dvm = this,
                mobileScreenSize = "xs";
            dvm.opened = !!$scope.opened;
            dvm.close = close;
            dvm.open = open;
            $rootScope.$on("best-way-open", setOpen);
            $rootScope.$on("best-way-close", setClose);

            function open() {
                if (dvm.opened) {
                    return
                }
                if (!screenSize.is(mobileScreenSize)) {
                    $rootScope.$emit("best-way-open")
                }
                setOpen()
            }

            function close($event) {
                $event.stopPropagation();
                if (!screenSize.is(mobileScreenSize)) {
                    $rootScope.$emit("best-way-close")
                }
                setClose()
            }

            function setOpen() {
                $element.find(".element .content").slideDown();
                dvm.opened = true
            }

            function setClose() {
                $element.find(".element .content").slideUp();
                dvm.opened = false
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("destinationCards", destinationCards);

        function destinationCards() {
            return {
                restrict: "A",
                scope: {},
                controller: destinationCardsController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                }
            }
        }
        destinationCardsController.$inject = ["$element", "$scope", "$timeout", "screenSize", "$analytics"];

        function destinationCardsController($element, $scope, $timeout, screenSize, $analytics) {
            var dvm = this;
            dvm.extended = false;
            dvm.bigBox = 1;
            dvm.expandCard = expandCard;
            dvm.seeMore = seeMore;
            dvm.showAllPrices = showAllPrices;
            dvm.showRestaurantPrices = showRestaurantPrices;
            dvm.weatherTemperatureUnit = "c";
            dvm.weatherIcon = "";
            dvm.weatherResume = null;
            $scope.$on("todaysWeather", function(event, data) {
                dvm.weatherIcon = data.weatherCode;
                $element.find(".getting-weather").fadeOut(1e3, function() {
                    dvm.weatherResume = data;
                    if ($scope.$root.$$phase !== "$apply" && $scope.$root.$$phase !== "$digest") {
                        $scope.$apply()
                    }
                    $timeout(function() {
                        $element.find(".todays-weather").fadeIn()
                    })
                })
            });
            $scope.$on("useDegreesCelsius", function(event, data) {
                dvm.weatherTemperatureUnit = data === true ? "c" : "f"
            });

            function expandCard(selectedIndex) {
                if (selectedIndex === dvm.bigBox) {
                    return
                }
                dvm.extended = $element.find(".card-" + selectedIndex + " .show-more").hasClass("extended") || false;
                dvm.bigBox = selectedIndex;
                scrollBar();
                fixScrollMobile(selectedIndex);
                $analytics.eventTrack("Expand card", {
                    category: "Destination Page",
                    label: $element.find(".card-" + selectedIndex + " .mod-title").text()
                })
            }

            function seeMore($event) {
                $event.stopPropagation();
                dvm.extended = !dvm.extended;
                scrollBar();
                fixScrollMobile(dvm.bigBox)
            }

            function scrollBar() {
                var target = $element.find(".card-" + dvm.bigBox + " .show-more");
                if (dvm.extended) {
                    $timeout(function() {
                        target.mCustomScrollbar()
                    })
                } else {
                    target.mCustomScrollbar("destroy");
                    angular.element(".text-content.show-more").scrollTop(0)
                }
            }

            function fixScrollMobile(selectedIndex) {
                var $body = $("html, body"),
                    $itemBoxSmall, $itemBox, $header, scrollTo, itemBoxSmallOuterHeight;
                if (screenSize.is("xs")) {
                    $itemBoxSmall = $(".city-item-box.small");
                    $itemBox = $(".city-item-box");
                    $header = $(".header-nav"), itemBoxSmallOuterHeight = $itemBoxSmall.first().outerHeight(true);
                    if (itemBoxSmallOuterHeight > 0) {
                        scrollTo = itemBoxSmallOuterHeight * (selectedIndex - 1) + $itemBox.first().offset().top - $header.outerHeight(true);
                        $body.animate({
                            scrollTop: scrollTo
                        }, 200)
                    }
                }
            }

            function showAllPrices() {
                var target = $element.find(".modal-body");
                target.find(".price-type").removeClass("hide").addClass("shown");
                target.mCustomScrollbar()
            }

            function showRestaurantPrices() {
                var target = $element.find(".modal-body");
                target.find(".transport-prices").removeClass("shown").addClass("hide");
                target.mCustomScrollbar()
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("howToGetThere", howToGetThere);

        function howToGetThere() {
            return {
                restrict: "A",
                controller: howToGetThereController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    type: "@"
                }
            }
        }
        howToGetThereController.$inject = ["$scope"];

        function howToGetThereController($scope) {
            var dvm = this;
            dvm.type = $scope.type;
            if (dvm.type === "tabs") {
                $scope.activeTab = 1
            } else {
                $scope.activeTab = [1]
            }
            dvm.setActiveTab = setActiveTab;
            dvm.isActiveTab = isActiveTab;

            function setActiveTab(tab) {
                var index, element;
                if (dvm.type === "tabs") {
                    if (tab !== $scope.activeTab) {
                        element = angular.element("div.tabs-type");
                        element.hide();
                        element.find(".tab-content").hide();
                        element = angular.element('div.tabs-type[data-content-loop="' + tab + '"]');
                        element.show();
                        element.find(".tab-content").fadeIn(500);
                        $scope.activeTab = tab
                    }
                } else {
                    index = $scope.activeTab.indexOf(tab);
                    angular.element('div.collapse-type[data-content-loop="' + tab + '"]').slideToggle(200);
                    if (index > -1) {
                        $scope.activeTab.splice(index, 1)
                    } else {
                        $scope.activeTab.push(tab)
                    }
                }
            }

            function isActiveTab(tab) {
                if (dvm.type === "tabs") {
                    return $scope.activeTab === tab
                } else {
                    return $scope.activeTab.indexOf(tab) !== -1
                }
            }

            function init() {
                var element;
                if (dvm.type === "tabs") {
                    element = angular.element("div.tabs-type:not(:first)");
                    element.hide();
                    element.find(".tab-content").hide()
                } else {
                    angular.element("div.collapse-type:not(:first)").hide()
                }
            }
            init()
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("offTheRoadDestinationCards", offTheRoadDestinationCards);

        function offTheRoadDestinationCards() {
            return {
                restrict: "A",
                scope: {},
                controller: offTheRoadDestinationCardsController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                }
            }
        }
        offTheRoadDestinationCardsController.$inject = ["$element", "$analytics"];

        function offTheRoadDestinationCardsController($element, $analytics) {
            var dvm = this;
            dvm.selectedOTRD = 1;
            dvm.selectedGetThere = 1;
            dvm.selectOTRD = selectOTRD;
            dvm.selectGetThere = selectGetThere;

            function selectOTRD(selectedIndex) {
                var selector;
                if (dvm.selectedOTRD !== selectedIndex) {
                    dvm.selectedOTRD = selectedIndex;
                    dvm.selectedGetThere = 1;
                    selector = ".otrd-big-cards .otr-" + dvm.selectedOTRD + " .box-item-title";
                    $analytics.eventTrack("Expand Off The Road Destination", {
                        category: "Destination Page",
                        label: $element.find(selector).text()
                    })
                }
            }

            function selectGetThere(selectedIndex) {
                var selector;
                if (dvm.selectedGetThere !== selectedIndex) {
                    dvm.selectedGetThere = selectedIndex;
                    selector = ".otrd-big-cards .otr-" + dvm.selectedOTRD + " .htgt-" + dvm.selectedGetThere + "  .subicon";
                    $analytics.eventTrack("Expand How to get There OTRD", {
                        category: "Destination Page",
                        label: $element.find(selector).text()
                    })
                }
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("regionSelector", ["$window", regionSelector]);

        function regionSelector($window) {
            return {
                restrict: "EA",
                scope: {},
                controller: regionSelectorController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                compile: function() {
                    return {
                        post: function(scope, element) {
                            var $w = angular.element($window);
                            $w.bind("resize", scope.dvm.checkCountryPositionHolder);
                            element.on("$destroy", function() {
                                $w.unbind("resize", scope.dvm.checkCountryPositionHolder)
                            })
                        }
                    }
                }
            }
        }

        function regionSelectorController() {
            var dvm = this,
                regionSelector = ".region-info",
                countrySelector = ".country",
                countryNameSelector = ".country-name",
                positionHolderClass = "position-holder",
                $selectedCountry, $positionHolder, activeClass = "active",
                hideClass = "hide",
                $body = $("html,body"),
                $selectRegion = $(".select-region");

            function scrollToElement($elem) {
                $body.animate({
                    scrollTop: $elem.offset().top - 15
                }, 600)
            }

            function checkCountryPositionHolder() {
                var countryHeight, holderHeight;
                countryHeight = $selectedCountry && $selectedCountry.outerHeight(true);
                holderHeight = $positionHolder && $positionHolder.height();
                if (countryHeight > 0 && holderHeight) {
                    $positionHolder.height(countryHeight)
                }
            }

            function createCountryPositionHolder(country) {
                var height;
                height = country.outerHeight(true);
                if (country.is(":visible") && height > 0) {
                    $selectedCountry = country;
                    $positionHolder = $("<div>").addClass(positionHolderClass).insertAfter(country).height(height)
                }
            }

            function clickCountry($event) {
                var elem = angular.element($event.currentTarget || $event.srcElement),
                    country = elem.next(countrySelector);
                angular.element("." + positionHolderClass).remove();
                angular.element(countryNameSelector).not(elem).removeClass(activeClass);
                elem.toggleClass(activeClass);
                angular.element(countrySelector).not(country).addClass(hideClass);
                country.toggleClass(hideClass);
                createCountryPositionHolder(country)
            }

            function clickRegion(regionSlug) {
                var region = angular.element(regionSelector + "." + regionSlug);
                angular.element(countryNameSelector).removeClass("active");
                angular.element(regionSelector).add(countrySelector).not(region).addClass(hideClass);
                region.toggleClass(hideClass);
                if (region && region.length > 0) {
                    scrollToElement(region)
                }
            }

            function closeRegion($event) {
                var elem = angular.element($event.currentTarget || $event.srcElement);
                elem.parents(regionSelector).addClass(hideClass);
                scrollToElement($selectRegion)
            }
            dvm.checkCountryPositionHolder = checkCountryPositionHolder;
            dvm.clickCountry = clickCountry;
            dvm.clickRegion = clickRegion;
            dvm.closeRegion = closeRegion
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("regionTabsSelector", regionTabsSelector);

        function regionTabsSelector() {
            return {
                restrict: "A",
                controller: regionTabsSelectorController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {}
            }
        }
        regionTabsSelectorController.$inject = ["$scope"];

        function regionTabsSelectorController($scope) {
            var dvm = this;
            dvm.setActiveTab = setActiveTab;
            dvm.isActiveTab = isActiveTab;
            dvm.setCollapsed = setCollapsed;
            dvm.isOpen = isOpen;
            dvm.collapsed = true;

            function setActiveTab(tab, selector) {
                var element;
                if (selector) {
                    element = angular.element('.tab-selector.select:not([data-id="' + tab + '"])');
                    element.slideUp()
                } else {
                    element = angular.element('.tab-selector.select:not([data-id="' + tab + '"])');
                    element.hide();
                    element = angular.element('.tab-selector.select[data-id="' + tab + '"]');
                    element.show()
                }
                if (tab !== $scope.activeTab) {
                    element = angular.element("div.tabs-type");
                    element.hide();
                    element.find(".tab-content").hide();
                    element = angular.element('div.tabs-type[data-id="' + tab + '"]');
                    element.show();
                    element.find(".tab-content").fadeIn(500);
                    $scope.activeTab = tab
                }
                if (selector) {
                    setCollapsed()
                }
            }

            function isActiveTab(tab) {
                return $scope.activeTab === tab
            }

            function setCollapsed() {
                var element;
                dvm.collapsed = !dvm.collapsed;
                if (isOpen()) {
                    element = angular.element(".tab-selector.select");
                    element.slideDown()
                }
            }

            function isOpen() {
                return !dvm.collapsed
            }

            function init() {
                var contentloop, element;
                contentloop = angular.element("li.tab-selector.active").data("id");
                $scope.activeTab = contentloop;
                element = angular.element('.tab-selector.select:not([data-id="' + contentloop + '"])');
                element.hide();
                element = angular.element('div.tabs-type:not([data-id="' + contentloop + '"])');
                element.hide()
            }
            init()
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("otRoadWeatherWidget", offTheRoadWeatherWidget);

        function offTheRoadWeatherWidget() {
            return {
                restrict: "A",
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    offTheRoadId: "=",
                    visible: "="
                },
                controller: offTheRoadWeatherWidgetController
            }
        }
        offTheRoadWeatherWidgetController.$inject = ["$scope", "$timeout", "consts", "$injector"];

        function offTheRoadWeatherWidgetController($scope, $timeout, consts, $injector) {
            var dvm = this;
            dvm.visible = false;
            dvm.weather = null;
            dvm.error = false;
            $scope.$watch("visible", function(newValue) {
                if (true === newValue && !dvm.weather) {
                    getWeatherData()
                }
            });

            function getWeatherData() {
                $injector.get("$http").get(getWeatherDataUrl(), {
                    headers: {
                        "X-Requested-Context": "partial"
                    }
                }).then(getWeatherSuccess).catch(reportError).finally(getWeatherFinally)
            }

            function getWeatherSuccess(response) {
                if (response.data) {
                    setWeatherData(response.data)
                } else {
                    reportError()
                }
            }

            function getWeatherFinally() {
                if (!dvm.weather) {
                    $timeout(getWeatherData, 20 * 1e3)
                }
            }

            function getWeatherDataUrl() {
                return consts.offTheRoadWeather.replace(/\/0$/, "/" + $scope.offTheRoadId)
            }

            function setWeatherData(data) {
                dvm.weather = data
            }

            function reportError() {
                dvm.error = true
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("weatherWidget", weatherWidget);

        function weatherWidget() {
            return {
                restrict: "A",
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    selectedMonth: "=",
                    visible: "@",
                    destinationId: "="
                },
                controller: WeatherWidgetController
            }
        }
        WeatherWidgetController.$inject = ["$scope", "$element", "$timeout", "consts", "$injector"];

        function WeatherWidgetController($scope, $element, $timeout, consts, $injector) {
            var dvm = this;
            dvm.deg = true;
            dvm.showingWeek = true;
            dvm.mobileActive = 0;
            dvm.selectedMonth = null;
            dvm.monthClasses = [];
            dvm.visible = false;
            dvm.weather = null;
            $scope.$watch("visible", function(newValue) {
                $timeout(function() {
                    dvm.visible = newValue === "true"
                }, 400)
            });
            dvm.setSelectedMonth = setSelectedMonth;
            dvm.showWeek = showWeek;
            dvm.showMonth = showMonth;
            dvm.selectedMonthMatch = selectedMonthMatch;
            dvm.initSlick = initSlick;
            dvm.setWeatherTab = setWeatherTab;
            dvm.setMonthMob = setMonthMob;
            dvm.useCelcius = useCelcius;
            init();

            function init() {
                dvm.setSelectedMonth($scope.selectedMonth);
                getWeatherData()
            }

            function useCelcius(value) {
                dvm.deg = value;
                $scope.$emit("useDegreesCelsius", value)
            }

            function getWeatherData() {
                $injector.get("$http").get(getWeatherDataUrl(), {
                    headers: {
                        "X-Requested-Context": "partial"
                    }
                }).then(getWeatherSuccess).finally(getWeatherFinally)
            }

            function getWeatherSuccess(response) {
                if (response.data) {
                    setWeatherData(response.data)
                }
            }

            function getWeatherFinally() {
                if (!dvm.weather) {
                    $timeout(getWeatherData, 20 * 1e3)
                }
            }

            function getWeatherDataUrl() {
                return consts.weather.replace(/\/0$/, "/" + $scope.destinationId)
            }

            function setWeatherData(data) {
                dvm.weather = data;
                $scope.$emit("todaysWeather", data.today)
            }

            function setSelectedMonth(value) {
                var i;
                dvm.selectedMonth = value;
                dvm.monthClasses[value] = "active";
                for (i = 1; i < value; i++) {
                    dvm.monthClasses[value - i] = "sibling-" + i
                }
                for (i = value + 1; i <= 12; i++) {
                    dvm.monthClasses[i] = "sibling-" + (i - value)
                }
            }

            function showWeek() {
                dvm.showingWeek = true
            }

            function showMonth() {
                dvm.showingWeek = false
            }

            function setMonthMob(monthNum) {
                dvm.selectedMonthMob = monthNum
            }

            function selectedMonthMatch(index) {
                return dvm.selectedMonthMob === index
            }

            function setWeatherTab(index) {
                dvm.mobileActive = index
            }

            function initSlick() {
                $timeout(function() {
                    $element.find("div.bar").on("click", function() {
                        dvm.setSelectedMonth(angular.element(this).data("monthIndex"))
                    })
                }, 100)
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("ui.bootstrap.datepicker").config(function($provide) {
            $provide.decorator("uibDatepickerDirective", function($delegate) {
                var directive = $delegate[0],
                    link = directive.link;
                directive.compile = function() {
                    return function(scope, element, attrs, ctrls) {
                        var datepickerCtrl, ngModelCtrl;
                        link.apply(this, arguments);
                        datepickerCtrl = ctrls[0];
                        ngModelCtrl = ctrls[1];
                        if (ngModelCtrl) {
                            scope.$on("dateRangeChanged", function refreshView() {
                                datepickerCtrl.refreshView()
                            })
                        }
                    }
                };
                return $delegate
            })
        })
    })();
    (function() {
        "use strict";
        angular.module("app").directive("flightForm", flightForm);

        function flightForm() {
            return {
                restrict: "E",
                template: function(element) {
                    return element.html()
                },
                controller: flightFormController,
                controllerAs: "flightForm",
                scope: true,
                compile: function() {}
            }
        }
        flightFormController.$inject = ["$element", "$scope", "$timeout"];

        function flightFormController($element, $scope, $timeout) {
            var dvm = this;
            dvm.isReturn = true;
            dvm.formSubmited = false;
            dvm.errorMessages = [];
            dvm.submit = submit;
            dvm.valid = valid;
            dvm.setReturn = setReturn;

            function submit($event) {
                var form = angular.element($element),
                    invalidAges = form.find('[name="c-a[]"] option[value="-1"]:selected');
                dvm.errorMessages = [];
                if (!valid()) {
                    $event.preventDefault();
                    scrollToMessage()
                } else {
                    invalidAges.parent().remove();
                    dvm.formSubmited = true
                }
            }

            function valid() {
                return validateRequiredFields() && validatePassengers()
            }

            function validateRequiredFields() {
                var flightSearchForm = $scope.flightSearch;
                if (flightSearchForm.$invalid) {
                    angular.forEach(flightSearchForm.$error, function(field) {
                        angular.forEach(field, function(errorField) {
                            errorField.$setTouched()
                        })
                    });
                    dvm.errorMessages.push("required");
                    return false
                }
                return true
            }

            function validatePassengers() {
                var flightSearchForm = $scope.flightSearch,
                    element = angular.element($element),
                    infantsSelectors = element.find('[name="c-a[]"] option[value="0"]:selected, [name="c-a[]"] option[value="1"]:selected'),
                    invalidAgeSelector = element.find(".child-age-selector:not(.hidden)").find(".age-selector-container.invalid");
                if (flightSearchForm.adults.$modelValue < infantsSelectors.length) {
                    dvm.errorMessages.push("passengers");
                    return false
                }
                if (invalidAgeSelector.length > 0) {
                    dvm.errorMessages.push("age");
                    return false
                }
                return true
            }

            function scrollToMessage() {
                var errorMessage = angular.element(".error-message").first(),
                    $body = $("html, body");
                $timeout(function() {
                    if (errorMessage.length) {
                        $body.animate({
                            scrollTop: errorMessage.offset().top - 100
                        }, 200)
                    }
                })
            }

            function setReturn(is) {
                dvm.isReturn = !!is
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("flightResults", flightResults);
        flightResultsController.$inject = ["$scope", "$timeout"];

        function flightResults() {
            return {
                restrict: "A",
                controller: flightResultsController,
                controllerAs: "flightResults",
                template: function(element) {
                    return element.html()
                },
                scope: {}
            }
        }

        function flightResultsController($scope, $timeout) {
            var dvm = this,
                body = angular.element("body");
            dvm.filtersOpen = false;
            dvm.onClick = onClick;
            $scope.$on("closeFilters", onClick);

            function onClick() {
                dvm.filtersOpen = !dvm.filtersOpen;
                if (dvm.filtersOpen) {
                    body.addClass("filters-shown");
                    $timeout(function() {
                        $scope.$broadcast("reCalcViewDimensions")
                    }, 10)
                } else {
                    body.removeClass("filters-shown")
                }
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("flightsResultsFilters", flightsResultsFilters);

        function flightsResultsFilters() {
            return {
                restrict: "A",
                controller: flightsResultsFiltersController,
                controllerAs: "flightsResultsFilters",
                template: function(element) {
                    return element.html()
                },
                scope: {}
            }
        }
        flightsResultsFiltersController.$inject = ["$scope", "$rootScope", "flightOffersFilter"];

        function flightsResultsFiltersController($scope, $rootScope, flightOffersFilter) {
            var dvm = this;
            dvm.flightOffersFilter = flightOffersFilter;
            dvm.filterData = null;
            dvm.show = show;
            dvm.moreAirlines = false;
            dvm.hideReturnTimeFilter = true;
            dvm.disableFilterAirports = {
                from: false,
                to: false
            };
            dvm.disableFilterAirlines = false;
            dvm.closeFilters = closeFilters;
            dvm.resetFilters = resetFilters;
            dvm.disableFilterStops = {
                direct: false,
                "1stop": false,
                "2stop": false
            };
            dvm.checkedFilterStops = {
                direct: false,
                "1stop": false,
                "2stop": false
            };
            dvm.changeFilters = function(filterName) {
                dvm.flightOffersFilter.set(filterName, dvm.flightOffersFilter.props[filterName]);
                applyFilters()
            };
            dvm.changeDictionaryFilters = function(keys, filterName) {
                var prop = dvm.flightOffersFilter.props[keys[0]];
                if (keys.length > 1) {
                    prop = prop[keys[1]]
                }
                dvm.flightOffersFilter.setDictionary(keys, filterName, prop[filterName]);
                applyFilters()
            };
            dvm.duration = generateSliderStructure(40, "duration", false);
            dvm.departureTime = generateSliderStructure(24, "departureTime", true);
            dvm.returnTime = generateSliderStructure(24, "returnTime", true);
            dvm.showAirlines = showAirlines;
            $rootScope.$on("getResultsFinished", resultsListener);
            $rootScope.$on("resetFilters", applyResetFilters);
            $rootScope.$on("updateSlider", updateSlider);

            function resetFilters() {
                $scope.$emit("resetFilters")
            }

            function applyResetFilters() {
                dvm.flightOffersFilter.reset();
                resetSlider("duration", false);
                resetSlider("departureTime", true);
                resetSlider("returnTime", true);
                applyFilters()
            }

            function resetSlider(modelName, isRange) {
                if (isRange) {
                    dvm[modelName].minValue = dvm[modelName].options.floor;
                    dvm[modelName].maxValue = dvm[modelName].options.ceil
                } else {
                    dvm[modelName].value = dvm[modelName].options.ceil
                }
            }

            function updateSlider($event, property, values) {
                var idx;
                for (idx in values) {
                    dvm[property][idx] = values[idx]
                }
            }

            function generateSliderStructure(maxValue, prop, isRange) {
                var values, sliderAttributes, conf;
                conf = {
                    options: {
                        floor: 0,
                        ceil: maxValue,
                        step: .5,
                        precision: 1,
                        enforceStep: false,
                        translate: function(value) {
                            return isRange ? convertToHHMM(value) : value + "h"
                        },
                        onEnd: function(sliderId, modelValue, highValue) {
                            values = isRange ? {
                                min: modelValue,
                                max: highValue
                            } : {
                                min: dvm[prop].options.floor,
                                max: modelValue
                            };
                            sliderAttributes = isRange ? {
                                minValue: modelValue,
                                maxValue: highValue
                            } : {
                                value: modelValue
                            };
                            $scope.$emit("updateSlider", prop, sliderAttributes);
                            dvm.flightOffersFilter.set(prop, values);
                            applyFilters()
                        }
                    }
                };
                if (isRange) {
                    conf.minValue = 0;
                    conf.maxValue = maxValue
                } else {
                    conf.value = maxValue;
                    conf.options.showSelectionBar = true
                }
                return conf
            }

            function resultsListener($event, filters) {
                if (!_.isEmpty(filters)) {
                    dvm.filterData = filters;
                    dvm.disableFilterAirports = {
                        from: "airports" in dvm.filterData && _.size(dvm.filterData.airports.from) === 1,
                        to: "airports" in dvm.filterData && _.size(dvm.filterData.airports.to) === 1
                    };
                    dvm.checkedFilterStops = {
                        direct: "stops" in dvm.filterData && isOnlyStop("direct"),
                        "1stop": "stops" in dvm.filterData && isOnlyStop("1stop"),
                        "2stops": "stops" in dvm.filterData && isOnlyStop("2stops")
                    };
                    dvm.disableFilterStops = {
                        direct: "stops" in dvm.filterData && (!dvm.filterData.stops["direct"] || isOnlyStop("direct")),
                        "1stop": "stops" in dvm.filterData && (!dvm.filterData.stops["1stop"] || isOnlyStop("1stop")),
                        "2stops": "stops" in dvm.filterData && (!dvm.filterData.stops["2stops"] || isOnlyStop("2stops"))
                    };
                    dvm.disableFilterAirlines = _.size(dvm.filterData.airlines) <= 1;
                    dvm.hideReturnTimeFilter = dvm.filterData.returnTimes.min === null && dvm.filterData.returnTimes.max === null;
                    dvm.duration.options.floor = dvm.filterData.duration.from;
                    dvm.duration.options.ceil = dvm.filterData.duration.to;
                    dvm.duration.value = dvm.filterData.duration.to;
                    airlinesFilter()
                }
            }

            function airlinesFilter() {
                var nCols, col, index, nAirlines = _.size(dvm.filterData.airlines);
                if (nAirlines === 0) {
                    return
                }
                dvm.nAirlines = dvm.filterData.airlines.length;
                if (nAirlines > 3) {
                    nCols = 3
                } else {
                    dvm.moreAirlines = true;
                    nCols = 1
                }
                dvm.ncols = nCols;
                dvm.itemsPerCol = Math.ceil(dvm.filterData.airlines.length / nCols);
                dvm.nColsArray = _.range(nCols);
                dvm.airlinesOrdered = [];
                for (col = 0; col < nCols; col++) {
                    dvm.airlinesOrdered[col] = []
                }
                _.each(dvm.filterData.airlines, function(airline, key) {
                    index = Math.floor(key / dvm.itemsPerCol);
                    dvm.airlinesOrdered[index].push(airline)
                })
            }

            function applyFilters() {
                $rootScope.$broadcast("applyFilters", dvm.flightOffersFilter.filterOffer)
            }

            function show($event) {
                var filterTitle = angular.element($event.currentTarget || $event.srcElement),
                    filter = filterTitle.parent(),
                    filterContent = filter.find(".filter-content");
                filterContent.slideToggle();
                filter.toggleClass("closed")
            }

            function showAirlines() {
                dvm.moreAirlines = true
            }

            function closeFilters() {
                $scope.$emit("closeFilters")
            }

            function convertToHHMM(value) {
                var hor = Math.floor(Math.abs(value)),
                    min = Math.floor(Math.abs(value) * 60 % 60);
                return (hor < 10 ? "0" : "") + hor + ":" + (min < 10 ? "0" : "") + min
            }

            function isOnlyStop(stopKey) {
                var key;
                for (key in dvm.filterData.stops) {
                    if (dvm.filterData.stops[key] && key !== stopKey) {
                        return false
                    }
                }
                return true
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("passengerForm", passengerForm);

        function passengerForm() {
            return {
                restrict: "A",
                template: function(element) {
                    return element.html()
                },
                controller: passengerFormController,
                controllerAs: "passengerForm",
                scope: {
                    gaProduct: "@",
                    currency: "@"
                },
                compile: function() {}
            }
        }
        passengerFormController.$inject = ["$element", "$scope", "$timeout", "analyticsEc", "Utils"];

        function passengerFormController($element, $scope, $timeout, analyticsEc, Utils) {
            var dvm = this,
                form = angular.element("#flights-passengers"),
                passenger, validForm, $body = $("html, body");
            dvm.copyAdultDetails = copyAdultDetails;
            dvm.uncheckAdultDetails = uncheckAdultDetails;
            dvm.isEmpty = isEmpty;
            dvm.copyAdultsModel = null;
            dvm.numberPassengers = angular.element(".passenger-details").length;
            dvm.isBirthdayValid = isBirthdayValid;
            dvm.departureDate = parseDate(form.find("#departure-date").val());
            dvm.returnDate = parseDate(form.find("#return-date").val());
            dvm.birthdayValid = [];
            dvm.formSubmited = false;
            dvm.isBirthdayTouched = isBirthdayTouched;
            dvm.formSubmit = formSubmit;
            dvm.setFieldClass = setFieldClass;
            dvm.setBirthdayClass = setBirthdayClass;
            dvm.setBirthdayFieldClass = setBirthdayFieldClass;
            init();

            function init() {
                formErrorsInit();
                trackingPage()
            }

            function formErrorsInit() {
                validForm = true;
                for (passenger = 1; passenger <= dvm.numberPassengers; passenger++) {
                    dvm.birthdayValid[passenger] = true
                }
                dvm.errorMessages = {
                    details: [],
                    contact: []
                }
            }

            function formSubmit($event) {
                formErrorsInit();
                validatePassengers();
                validateRequiredFields();
                if (!validForm) {
                    $event.preventDefault();
                    scrollToMessage()
                } else {
                    dvm.formSubmited = true;
                    scrollTo(angular.element(".booking-steps-container").offset().top - 100);
                    trackingSubmit()
                }
            }

            function scrollTo(position) {
                $body.animate({
                    scrollTop: position
                }, 200)
            }

            function scrollToMessage() {
                var errorMessage;
                $timeout(function() {
                    errorMessage = angular.element(".error-message").first();
                    if (errorMessage.length) {
                        scrollTo(errorMessage.offset().top - 100)
                    }
                })
            }

            function validateRequiredFields() {
                var passengersInfoForm = $scope.passengersInfo;
                if (passengersInfoForm.$invalid) {
                    angular.forEach(passengersInfoForm.$error, function(field) {
                        angular.forEach(field, function(errorField) {
                            setRequiredErrorMessages(errorField.$name, errorField.$error);
                            errorField.$setTouched()
                        })
                    });
                    validForm = false
                }
            }

            function setRequiredErrorMessages(name, error) {
                var subStr, section, num;
                if (error.required || error.pattern || error.email) {
                    subStr = name.split("-");
                    section = subStr.shift();
                    num = subStr.pop();
                    if (section === "contact") {
                        dvm.errorMessages["contact"].push("required")
                    } else {
                        dvm.errorMessages["details"][num].push("required")
                    }
                }
                if (error.conditions) {
                    dvm.errorMessages["contact"].push("conditions")
                }
            }

            function uncheckAdultDetails(adultNumber) {
                var element = $element.find("#contact-copy-details-" + adultNumber),
                    enabled = element.is(":checked");
                if (!element.data("auto-changed") && enabled) {
                    element.removeAttr("checked").checked = false;
                    dvm.copyAdultsModel = null
                }
                element.removeData("auto-changed")
            }

            function copyAdultDetails(adultNumber) {
                var form = $scope.dvm.form,
                    gender = "passengerGenderDetails",
                    name = "passengerNameDetails",
                    surname = "passengerSurnameDetails",
                    element = $element.find("#contact-copy-details-" + adultNumber),
                    enabled = element.is(":checked");
                gender += adultNumber;
                name += adultNumber;
                surname += adultNumber;
                if (enabled) {
                    element.data("auto-changed", true);
                    form.contactGenderDetails = getContactValue(adultNumber, form[gender], "#contact-gender-details option", "#passenger-gender-details-");
                    form.contactNameDetails = form[name];
                    form.contactSurnameDetails = form[surname]
                }
            }

            function getContactValue(adultNumber, genderValue, contactSelector, passengerSelector) {
                var genderText = getPassengerText(adultNumber, genderValue, passengerSelector);
                return $(contactSelector).filter(function() {
                    return $(this).text().trim() === genderText
                }).val()
            }

            function getPassengerText(adultNumber, value, passengerSelector) {
                var selector = passengerSelector + adultNumber + ' option[value="' + value + '"]';
                return $(selector).text().trim()
            }

            function isEmpty(field) {
                return $scope.passengersInfo[field].$viewValue === ""
            }

            function validatePassengers() {
                for (passenger = 1; passenger <= dvm.numberPassengers; passenger++) {
                    dvm.errorMessages.details[passenger] = [];
                    isBirthdayValid(passenger, true)
                }
            }

            function isBirthdayValid(passenger, showMsg) {
                var valid = checkBirthday(passenger);
                dvm.birthdayValid[passenger] = valid.result;
                if (valid.msg && showMsg) {
                    dvm.errorMessages["details"][passenger].push(valid.msg);
                    validForm = false
                }
            }

            function isBirthdayTouched(passenger) {
                return $scope.passengersInfo["passenger-birthday-year-details-" + passenger].$touched || $scope.passengersInfo["passenger-birthday-month-details-" + passenger].$touched || $scope.passengersInfo["passenger-birthday-day-details-" + passenger].$touched
            }

            function checkBirthday(passenger) {
                var adultAge = 18,
                    birthdayDate, ageAtReturnDate, type = form.find("#passenger-type-" + passenger),
                    birthdayDay = form.find("#passenger-birthday-day-details-" + passenger),
                    birthdayMonth = form.find("#passenger-birthday-month-details-" + passenger),
                    birthdayYear = form.find("#passenger-birthday-year-details-" + passenger),
                    childrenAge = form.find("#passenger-child-age-" + passenger),
                    birthdayHidden = angular.element("#passenger-birthday-details-" + passenger),
                    result = true,
                    msg = "";
                birthdayDate = new Date(birthdayYear.val(), birthdayMonth.val() - 1, birthdayDay.val());
                ageAtReturnDate = Utils.calculateAge(birthdayDate, dvm.returnDate);
                if (birthdayYear.val() === "" || birthdayMonth.val() === "" || birthdayDay.val() === "") {
                    result = false
                } else if (type.val() === "adults" && ageAtReturnDate < adultAge) {
                    result = false;
                    msg = "no-adult"
                } else if (type.val() === "children" && ageAtReturnDate >= adultAge) {
                    result = false;
                    msg = "no-child"
                } else if (type.val() === "children" && ageAtReturnDate !== parseInt(childrenAge.val())) {
                    result = false;
                    msg = "child-age-not-match"
                }
                birthdayHidden.val(birthdayYear.val() + "-" + birthdayMonth.val() + "-" + birthdayDay.val());
                return {
                    result: result,
                    msg: msg
                }
            }

            function setFieldClass(field) {
                return setClass($scope.passengersInfo[field].$invalid, $scope.passengersInfo[field].$touched)
            }

            function setBirthdayClass(passenger) {
                return setClass(!dvm.birthdayValid[passenger], dvm.isBirthdayTouched(passenger))
            }

            function setBirthdayFieldClass(field, passenger) {
                return setClass(!dvm.birthdayValid[passenger], $scope.passengersInfo[field].$touched)
            }

            function setClass(isInvalid, isTouched) {
                var classes = [],
                    errorClass = "error",
                    touchedClass = "touched";
                if (isInvalid) {
                    classes.push(errorClass)
                }
                if (isTouched) {
                    classes.push(touchedClass)
                }
                return classes.join(" ")
            }

            function parseDate(dateStr) {
                var dateParts = dateStr.split("-");
                return new Date(dateParts[0], dateParts[1] - 1, dateParts[2])
            }

            function trackingPage() {
                analyticsEc.trackPageview($scope.currency, $scope.gaProduct, "Passenger details")
            }

            function trackingSubmit() {
                analyticsEc.trackCheckout($scope.currency, $scope.gaProduct, {
                    step: "Passenger details"
                })
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("paymentForm", paymentForm);

        function paymentForm() {
            return {
                restrict: "A",
                template: function(element) {
                    return element.html()
                },
                controller: paymentFormController,
                controllerAs: "paymentForm",
                scope: {
                    spreedlyApiKey: "@",
                    gaProduct: "@",
                    currency: "@"
                },
                compile: function() {}
            }
        }
        paymentFormController.$inject = ["$scope", "$timeout", "analyticsEc"];

        function paymentFormController($scope, $timeout, analyticsEc) {
            var dvm = this,
                inputStyle = "font-weight:normal;font-family:'Open Sans',sans-serif;",
                validStyle = "color:#394049",
                errorStyle = "color:#a94442";
            dvm.focusOn = {
                number: false,
                cvv: false
            };
            dvm.lengthInput = {
                number: 0,
                cvv: 0
            };
            dvm.validCard = {
                number: false,
                cvv: false
            };
            dvm.touchedCard = {
                number: false,
                cvv: false
            };
            dvm.cardName = "";
            dvm.cardType = "";
            dvm.formSubmited = false;
            dvm.errorMessages = [];
            dvm.spreedly = new SpreedlyPaymentFrame;
            dvm.checkValidCard = checkValidCard;
            dvm.checkName = checkName;
            dvm.checkExpiryDate = checkExpiryDate;
            dvm.submit = submit;
            init();

            function init() {
                trackingPage()
            }
            inputStyle += "font-size:16px;height:30px;line-height:30px;width:100%";
            dvm.spreedly.init($scope.spreedlyApiKey, {
                numberEl: "spreedly-number",
                cvvEl: "spreedly-cvv"
            });
            dvm.spreedly.on("ready", function() {
                dvm.spreedly.setPlaceholder("number", "");
                dvm.spreedly.setStyle("number", inputStyle + validStyle);
                dvm.spreedly.setPlaceholder("cvv", "");
                dvm.spreedly.setStyle("cvv", inputStyle + validStyle);
                dvm.spreedly.transferFocus("number")
            });
            dvm.spreedly.on("validation", function(inputProperties) {
                checkValidCard(inputProperties)
            });
            dvm.spreedly.on("fieldEvent", function(name, type, activeEl, inputProperties) {
                if (type === "input") {
                    checkValidCard(inputProperties)
                }
                if (type === "blur") {
                    dvm.touchedCard[name] = true
                }
                resetPlaceholder(name, type, inputProperties)
            });
            dvm.spreedly.on("paymentMethod", function(token) {
                angular.element("#payment-method-token").val(token);
                angular.element("#payment-form").submit()
            });

            function checkValidCard(inputProperties) {
                if (_.size(inputProperties) > 0) {
                    dvm.validCard["number"] = inputProperties["validNumber"];
                    dvm.validCard["cvv"] = inputProperties["validCvv"];
                    dvm.cardName = dvm.validCard["number"] ? "credit-card-" + inputProperties["cardType"] : "";
                    dvm.cardType = inputProperties["cardType"];
                    if (typeof inputProperties["activeElement"] !== "undefined") {
                        setStyle("number");
                        setStyle("cvv")
                    }
                    scopeApply()
                }
            }

            function setStyle(field) {
                var fieldStyle;
                fieldStyle = dvm.validCard[field] ? validStyle : errorStyle;
                dvm.spreedly.setStyle(field, fieldStyle)
            }

            function checkName() {
                return !_.isEmpty(angular.element("#full-name").val())
            }

            function checkExpiryDate() {
                var currentDate, selectedMonth, selectedYear;
                currentDate = new Date;
                selectedYear = parseInt(angular.element("#expiry-year").val(), 10);
                selectedMonth = parseInt(angular.element("#expiry-month").val(), 10);
                if (selectedYear > currentDate.getFullYear() && selectedMonth >= 1) {
                    return true
                }
                if (selectedYear === currentDate.getFullYear() && selectedMonth >= currentDate.getMonth() + 1) {
                    return true
                }
                return false
            }

            function resetPlaceholder(name, type, inputProperties) {
                if (type === "focus") {
                    dvm.spreedly.setPlaceholder(name, "");
                    dvm.focusOn[name] = true
                }
                if (type === "input" && typeof inputProperties[name + "Length"] !== "undefined") {
                    dvm.lengthInput[name] = inputProperties[name + "Length"]
                }
                if (type === "blur") {
                    dvm.focusOn[name] = dvm.lengthInput[name] > 0 ? true : false
                }
                scopeApply()
            }

            function valid() {
                var result = true;
                dvm.spreedly.validate();
                if (!checkName()) {
                    setError("full_name", true);
                    result = false
                }
                if (!checkExpiryDate()) {
                    setError("year", true);
                    result = false
                }
                if (!dvm.validCard["number"]) {
                    setError("number", false);
                    result = false
                }
                if (!dvm.validCard["cvv"]) {
                    setError("cvv", false);
                    result = false
                }
                return result
            }

            function scopeApply() {
                if ($scope.$root.$$phase !== "$apply" && $scope.$root.$$phase !== "$digest") {
                    $scope.$apply()
                }
            }

            function submit($event) {
                dvm.errorMessages = [];
                if (!valid()) {
                    $event.preventDefault();
                    scrollToMessage();
                    return
                } else {
                    tokenizeCreditCard();
                    dvm.formSubmited = true;
                    trackingSubmit()
                }
            }

            function tokenizeCreditCard() {
                var requiredFields = {};
                requiredFields["full_name"] = angular.element("#full-name").val();
                requiredFields["month"] = angular.element("#expiry-month").val();
                requiredFields["year"] = angular.element("#expiry-year").val();
                dvm.spreedly.tokenizeCreditCard(requiredFields)
            }

            function scrollToMessage() {
                var errorMessage = angular.element(".error-message").first(),
                    $body = $("html, body");
                $timeout(function() {
                    if (errorMessage.length) {
                        $body.animate({
                            scrollTop: errorMessage.offset().top - 100
                        }, 200)
                    }
                })
            }

            function setError(field, setTouched) {
                dvm.errorMessages.push(field);
                if (setTouched) {
                    $scope.paymentInfo[field].$setTouched()
                } else {
                    dvm.touchedCard[field] = true
                }
            }

            function trackingPage() {
                analyticsEc.trackPageview($scope.currency, $scope.gaProduct, "Payment")
            }

            function trackingSubmit() {
                analyticsEc.trackCheckout($scope.currency, $scope.gaProduct, {
                    step: "Payment",
                    option: dvm.cardType
                })
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("airportSelector", [airportSelector]);

        function airportSelector() {
            return {
                restrict: "A",
                controller: airportSelectorController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    airportLocation: "@",
                    initial: "="
                }
            }
        }
        airportSelectorController.$inject = ["$scope", "LocationSuggestionService"];

        function airportSelectorController($scope, LocationSuggestionService) {
            var lastValidAirport, dvm = this;
            dvm.selectedAirport = null;
            dvm.selectDestination = selectDestination;
            dvm.suggestAirport = suggestAirport;
            dvm.airportValidation = airportValidation;
            dvm.suggestionLabel = suggestionLabel;
            dvm.inputFormatter = inputFormatter;
            if (_.isObject($scope.initial) && $scope.initial.code && $scope.initial.name) {
                $scope.initial.name = decodeURIComponent($scope.initial.name);
                dvm.selectedAirport = lastValidAirport = $scope.initial
            }

            function suggestAirport(text) {
                return LocationSuggestionService.searchAirport(text).then(function(response) {
                    return checkAirport(response.data)
                }).catch(function(error) {
                    console.error(error);
                    return []
                })
            }

            function checkAirport(data) {
                var name = $scope.airportLocation === "origin" ? "t" : "f";
                return _.filter(data, function(o) {
                    return o.code !== $('input[name="' + name + '"]').val()
                })
            }

            function selectDestination($item) {
                if (!$item || !$item.code) {
                    return
                }
                dvm.selectedAirport = lastValidAirport = $item;
                $scope.$emit($scope.airportLocation + "Value", $item)
            }

            function airportValidation() {
                if (!dvm.selectedAirport && lastValidAirport) {
                    dvm.selectedAirport = lastValidAirport
                }
            }

            function inputFormatter($model) {
                if (!$model) {
                    return
                }
                return $model.name + " (" + $model.code + ")"
            }

            function suggestionLabel(address) {
                var label, icon, outerClass = "";
                if (!address || address.error) {
                    return address && address.error
                }
                icon = "flight";
                if (address && address.city) {
                    icon = "city"
                }
                if (address && address.child) {
                    outerClass = "is-child"
                }
                label = {
                    addressCode: address.code,
                    addressName: address.name,
                    addressCountry: address.country,
                    outerClass: outerClass,
                    icon: icon
                };
                return label
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("bindHtmlCompile", ["$compile", function($compile) {
            return {
                restrict: "A",
                link: function(scope, element, attrs) {
                    scope.$watch(function() {
                        return scope.$eval(attrs.bindHtmlCompile)
                    }, function(value) {
                        element.html(value);
                        $compile(element.contents())(scope)
                    })
                }
            }
        }])
    })();
    (function() {
        "use strict";
        angular.module("app").directive("bookingDatePicker", datePicker);

        function datePicker() {
            return {
                restrict: "EA",
                controller: datePickerController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    minDate: "@",
                    departureDate: "@",
                    returnDate: "@"
                }
            }
        }
        datePickerController.$inject = ["$scope", "$rootScope", "uibDatepickerConfig"];

        function datePickerController($scope, $rootScope, uibDatepickerConfig) {
            var dvm = this;
            dvm.config = uibDatepickerConfig;
            dvm.departure_opened = false;
            dvm.returning_opened = false;
            dvm.toggleDepartureOpened = toggleDepartureOpened;
            dvm.toggleReturningOpened = toggleReturningOpened;
            dvm.min_date = new Date($scope.minDate);
            dvm.departure_date = new Date;
            dvm.return_date = new Date;
            dvm.departureDateRange = {
                minDate: dvm.min_date,
                maxDate: new Date(dvm.config.maxDate)
            };
            dvm.returnDateRange = {
                minDate: dvm.min_date,
                maxDate: new Date(dvm.config.maxDate)
            };
            $scope.$watch("dvm.departure_date", function() {
                recalculateReturningMinDate()
            });

            function toggleDepartureOpened() {
                dvm.departure_opened = !dvm.departure_opened
            }

            function toggleReturningOpened() {
                dvm.returning_opened = !dvm.returning_opened
            }
            $rootScope.$on("openDate", openDate);

            function init() {
                setInitialDates()
            }

            function recalculateReturningMinDate() {
                dvm.returnDateRange.minDate = new Date(dvm.departure_date);
                if (dvm.return_date && new Date(dvm.departure_date).getTime() > new Date(dvm.return_date).getTime()) {
                    dvm.return_date = dvm.departure_date
                }
            }

            function setInitialDates() {
                dvm.departure_date = setDepartureDate();
                dvm.return_date = setReturnDate()
            }

            function setDepartureDate() {
                var date = new Date($scope.minDate);
                if ($scope.departureDate && date.setHours(0, 0, 0, 0) <= new Date($scope.departureDate).getTime()) {
                    return new Date($scope.departureDate)
                }
                return new Date($scope.minDate)
            }

            function setReturnDate() {
                var date = new Date(dvm.departure_date);
                if ($scope.returnDate && dvm.departure_date.setHours(0, 0, 0, 0) <= new Date($scope.returnDate).getTime()) {
                    return new Date($scope.returnDate)
                }
                return new Date(date.setDate(date.getDate() + 1))
            }

            function openDate(e, step) {
                if (step === "departure") {
                    dvm.departure_opened = true
                } else {
                    dvm.returning_opened = true
                }
            }
            init()
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("bookingMobileSteps", bookingMobileSteps);

        function bookingMobileSteps() {
            return {
                restrict: "A",
                controller: bookingMobileStepsController,
                controllerAs: "bookingMobileSteps",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    departure: "@",
                    destination: "@",
                    d: "@",
                    r: "@",
                    adults: "@",
                    children: "@",
                    minDate: "@"
                }
            }
        }
        bookingMobileStepsController.$inject = ["$scope", "$timeout", "$element", "uibDatepickerConfig"];

        function bookingMobileStepsController($scope, $timeout, $element, uibDatepickerConfig) {
            var dvm = this,
                isFormShown = true,
                $stepPath = $("#step-path"),
                $fields = $("#fields"),
                $body = $("body"),
                overlayStepSel = ".overlay-step",
                overlayStepsMobileClass = "overlay-steps-mobile",
                dpRenderOnEvent = "dateRangeChanged",
                bookingFieldValues = {},
                currentStepIndex = 0,
                passengers = {},
                datepickerConfig = uibDatepickerConfig,
                datepickerOptions = {
                    minDate: new Date($scope.minDate),
                    maxDate: new Date(uibDatepickerConfig.maxDate),
                    customClass: addDatepickerCustomClasses
                },
                steps = ["origin", "destination", "departureDate", "returnDate", "passengers"],
                render = ["origin", "destination", "date", "date", "passengers"],
                oldReturnDate;

            function setInitial() {
                dvm.isReturn = $scope.r || !$scope.d ? true : false;
                dvm.origin = decodeURIComponent($scope.departure) || "";
                dvm.destination = decodeURIComponent($scope.destination) || "";
                dvm.departureDate = getDepartureDate();
                dvm.returnDate = getReturnDate();
                dvm.date = dvm.departureDate;
                dvm.passengers = {
                    adults: $scope.adults || 1,
                    children: $scope.children || 0
                };
                if (dvm.origin === "") {
                    editField("origin")
                } else if (dvm.destination === "") {
                    editField("destination")
                } else {
                    selectDate();
                    dvm.isFormShown = true
                }
            }

            function getDepartureDate() {
                var date = new Date($scope.minDate);
                if ($scope.d && date.setHours(0, 0, 0, 0) <= new Date($scope.d).getTime()) {
                    return new Date($scope.d)
                }
                return date
            }

            function getReturnDate() {
                var date = new Date(dvm.departureDate);
                if ($scope.r && dvm.departureDate.setHours(0, 0, 0, 0) <= new Date($scope.r).getTime()) {
                    return new Date($scope.r)
                }
                return new Date(date.setDate(date.getDate() + 1))
            }

            function changeProgressOnScroll(event, yOffset) {
                dvm.scrolled = yOffset >= 90;
                if ($scope.$root.$$phase !== "$apply" && $scope.$root.$$phase !== "$digest") {
                    $scope.$apply()
                }
            }

            function showForm() {
                dvm.isFormShown = true;
                setStepIndex(0);
                $body.removeClass(overlayStepsMobileClass);
                $timeout(function() {
                    checkBlank("f", "origin");
                    checkBlank("t", "destination")
                })
            }

            function hideForm() {
                dvm.isFormShown = false;
                $body.addClass(overlayStepsMobileClass);
                $fields.animate({
                    scrollTop: $body.offset().top
                }, 100)
            }

            function editField(step) {
                var stepIndex = steps.indexOf(step) + 1;
                hideForm();
                setStepIndex(stepIndex);
                setField(stepIndex)
            }

            function setStepIndex(stepIndex) {
                $stepPath.children("li").each(function(index) {
                    index === stepIndex ? $(this).show() : $(this).hide()
                });
                dvm.currentStepIndex = stepIndex
            }

            function setField(stepIndex) {
                var currentStep = $fields.find(overlayStepSel + "-" + render[stepIndex - 1]);
                $fields.find(overlayStepSel).hide();
                currentStep.show();
                $timeout(function() {
                    setCursor(currentStep.find(":text").first())
                })
            }

            function setCursor(input) {
                if (input.val()) {
                    input.select()
                } else {
                    input.focus()
                }
            }

            function prevField() {
                if (dvm.currentStepIndex > 1) {
                    editField(steps[dvm.currentStepIndex - 2])
                }
            }

            function nextField() {
                if (dvm.currentStepIndex < steps.length) {
                    editField(steps[dvm.currentStepIndex])
                }
            }

            function checkNext() {
                if (!dvm[steps[dvm.currentStepIndex]]) {
                    nextField()
                } else {
                    showForm()
                }
            }

            function selectDate() {
                dvm[steps[dvm.currentStepIndex - 1]] = dvm.date;
                if (dvm.isReturn && dvm.departureDate && dvm.returnDate && dvm.departureDate.getTime() > dvm.returnDate.getTime()) {
                    if (steps[dvm.currentStepIndex - 1] === "departureDate") {
                        editField("returnDate");
                        dvm.returnDate = null
                    } else {
                        editField("departureDate");
                        dvm.departureDate = null
                    }
                    return
                }
                if (!dvm.isReturn) {
                    oldReturnDate = dvm.returnDate;
                    dvm.returnDate = null;
                    dvm.currentStepIndex++
                }
                $scope.$broadcast(dpRenderOnEvent);
                $timeout(function() {
                    checkNext()
                }, 500)
            }

            function addDatepickerCustomClasses(data) {
                var date = data.date,
                    mode = data.mode,
                    dayToCheck, newClass = "";
                if (mode === "day") {
                    dayToCheck = new Date(date).setHours(0, 0, 0, 0);
                    if (dvm.departureDate && dayToCheck === new Date(dvm.departureDate).setHours(0, 0, 0, 0)) {
                        newClass = "dp-departure-date linearicon"
                    }
                    if (dvm.isReturn && dvm.returnDate && dayToCheck === new Date(dvm.returnDate).setHours(0, 0, 0, 0)) {
                        newClass = "dp-return-date linearicon"
                    }
                    if (dvm.isReturn && dvm.returnDate && dvm.departureDate && dayToCheck < new Date(dvm.returnDate).setHours(0, 0, 0, 0) && dayToCheck > new Date(dvm.departureDate).setHours(0, 0, 0, 0)) {
                        newClass = "dp-interval-date"
                    }
                }
                return newClass
            }

            function submit(event) {
                var form = angular.element(event.target),
                    ageInputName = "c-a[]",
                    elem;
                angular.element("input[name=f]", form).val($('input[name="f"]').val());
                angular.element("input[name=t]", form).val($('input[name="t"]').val());
                angular.forEach(angular.element($element).find('[name="' + ageInputName + '"]'), function(value) {
                    elem = angular.element(value);
                    if (elem.val() >= 0) {
                        $("<input>").attr({
                            type: "hidden",
                            name: ageInputName,
                            value: elem.val()
                        }).appendTo(form)
                    }
                })
            }

            function setReturn(is) {
                dvm.isReturn = is;
                if (dvm.isReturn && dvm.departureDate && oldReturnDate && !dvm.returnDate) {
                    dvm.returnDate = oldReturnDate;
                    if (dvm.departureDate.getTime() > dvm.returnDate.getTime()) {
                        dvm.returnDate = dvm.departureDate
                    }
                }
                $scope.$broadcast(dpRenderOnEvent)
            }

            function checkBlank(field, airport) {
                if ($('input[name="' + field + '"]').val() === "") {
                    dvm[airport] = ""
                }
            }

            function init() {
                setInitial()
            }
            $scope.$on("originValue", function(e, data) {
                dvm.origin = data.name;
                checkNext()
            });
            $scope.$on("destinationValue", function(e, data) {
                dvm.destination = data.name;
                checkNext()
            });
            $scope.$on("passengersValue", function(e, data) {
                dvm.passengers = data;
                showForm()
            });
            $scope.$on("scrolling", changeProgressOnScroll);
            dvm.scrolled = null;
            dvm.isFormShown = isFormShown;
            dvm.passengers = passengers;
            dvm.showForm = showForm;
            dvm.hideForm = hideForm;
            dvm.editField = editField;
            dvm.prevField = prevField;
            dvm.setReturn = setReturn;
            dvm.dpRenderOnEvent = dpRenderOnEvent;
            dvm.currentStepIndex = currentStepIndex;
            dvm.bookingFieldValues = bookingFieldValues;
            dvm.datepickerConfig = datepickerConfig;
            dvm.datepickerOptions = datepickerOptions;
            dvm.selectDate = selectDate;
            dvm.submit = submit;
            init()
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("citySelector", [citySelector]);

        function citySelector() {
            return {
                restrict: "A",
                controller: citySelectorController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {}
            }
        }
        citySelectorController.$inject = ["LocationSuggestionService"];

        function citySelectorController(LocationSuggestionService) {
            var dvm = this;
            dvm.selectedCity = null;
            dvm.selectCity = selectCity;
            dvm.suggestCity = suggestCity;

            function suggestCity(text) {
                return LocationSuggestionService.searchCity(text).then(function(response) {
                    return response.data
                }).catch(function(error) {
                    console.error(error);
                    return []
                })
            }

            function selectCity($item) {
                dvm.selectedCity = $item
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("confirmationTracking", confirmationTracking);

        function confirmationTracking() {
            return {
                restrict: "A",
                controller: confirmationTrackingController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    id: "@",
                    revenue: "@",
                    gaProduct: "@",
                    currency: "@"
                }
            }
        }
        confirmationTrackingController.$inject = ["$scope", "analyticsEc"];

        function confirmationTrackingController($scope, analyticsEc) {
            init();

            function init() {
                trackingPage()
            }

            function trackingPage() {
                analyticsEc.trackPurchase($scope.currency, $scope.gaProduct, {
                    id: $scope.id,
                    affiliation: "Travioor",
                    revenue: $scope.revenue
                }, $scope.id)
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("moreDetails", moreDetails);

        function moreDetails() {
            return {
                restrict: "A",
                controller: moreDetailsController,
                controllerAs: "dvm",
                scope: {},
                template: function(element) {
                    return element.html()
                }
            }
        }
        moreDetailsController.$inject = ["$scope", "$timeout", "$window", "$element"];

        function moreDetailsController($scope, $timeout, $window, $element) {
            var dvm = this,
                $body = $("html, body"),
                selectUrl = $element.find("a").attr("href");
            dvm.moreDetails = moreDetails;
            dvm.select = select;

            function select($event, position) {
                $event.preventDefault();
                $scope.$emit("flight-selected", {
                    position: position
                });
                $timeout(function() {
                    $window.location.href = selectUrl
                })
            }

            function moreDetails($event) {
                var button = angular.element($event.currentTarget || $event.srcElement),
                    itinerary = button.closest(".itinerary"),
                    details = itinerary.find(".departure-details"),
                    current = angular.element(".itinerary.open"),
                    currentDetails = current.find(".departure-details"),
                    currentButton = current.find(".more-details");
                if (itinerary.hasClass("open")) {
                    button.removeClass("active");
                    details.slideUp(function() {
                        current.removeClass("open")
                    })
                } else {
                    currentDetails.slideUp();
                    button.addClass("active");
                    details.slideDown(function() {
                        itinerary.addClass("open");
                        current.removeClass("open");
                        currentButton.removeClass("active");
                        $body.animate({
                            scrollTop: angular.element(itinerary).offset().top - 100
                        }, 200)
                    })
                }
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("passengerSelector", ["$document", passengerSelector]);

        function passengerSelector($document) {
            return {
                restrict: "AE",
                controller: passengerSelectorController,
                controllerAs: "dvm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    maxPassengers: "=",
                    minAdults: "=",
                    minChildren: "=",
                    infantsLimitAge: "=",
                    passengerLiteral: "@?",
                    originId: "=",
                    onChange: "&",
                    adults: "=",
                    children: "=",
                    showDropdown: "="
                },
                compile: function() {
                    return {
                        pre: function(scope) {
                            scope.dvm.onChangeCallback = scope.onChange();
                            scope.setInitialParameters()
                        },
                        post: function(scope, element) {
                            function hideDropDownIfNecessary(event) {
                                var isInside = element.find(event.target).length > 0;
                                if (!scope.checkChildrenAges() || isInside) {
                                    return
                                }
                                scope.dvm.isDropdownVisible = scope.showDropdown || false;
                                if (scope.$root.$$phase !== "$apply" && scope.$root.$$phase !== "$digest") {
                                    scope.$apply()
                                }
                            }
                            $document.bind("click", hideDropDownIfNecessary);
                            scope.$on("$destroy", function() {
                                $document.unbind("click", hideDropDownIfNecessary)
                            })
                        }
                    }
                }
            }
        }
        passengerSelectorController.$inject = ["$scope", "$timeout", "$element"];

        function passengerSelectorController($scope, $timeout, $element) {
            var dvm = this;
            $scope.setInitialParameters = function() {
                dvm.adults = $scope.adults >= $scope.minAdults && $scope.adults + $scope.children <= $scope.maxPassengers ? $scope.adults : $scope.minAdults;
                dvm.children = $scope.adults + $scope.children <= $scope.maxPassengers ? $scope.children : $scope.minChildren;
                updatePassengerText();
                disable()
            };
            $scope.$watch("originId", function() {
                $timeout(function() {
                    $scope.setInitialParameters()
                }, 10)
            });
            $scope.checkChildrenAges = function() {
                var elem, parent, infantsCount = 0,
                    selector = '[name="c-a[]"]',
                    errorClass = "invalid",
                    result = true;
                if (dvm.isDropdownVisible && dvm.children >= 0) {
                    angular.forEach(angular.element($element).find(selector), function(value, key) {
                        elem = angular.element(value);
                        if (key < dvm.children) {
                            parent = elem.parent();
                            if (elem.val() < 0) {
                                parent.addClass(errorClass);
                                result = false
                            } else if (elem.val() < $scope.infantsLimitAge) {
                                infantsCount++;
                                if (infantsCount > dvm.adults) {
                                    parent.addClass(errorClass);
                                    result = false
                                } else {
                                    parent.removeClass(errorClass)
                                }
                            } else {
                                parent.removeClass(errorClass)
                            }
                        } else {
                            elem.val(-1)
                        }
                    })
                }
                return result
            };

            function updatePassengerText() {
                var passengerSum = _.sum([dvm.adults, dvm.children]);
                dvm.passengers = passengerSum + " " + $scope.passengerLiteral
            }

            function resolvePassengers() {
                if (dvm.onChangeCallback) {
                    dvm.onChangeCallback($scope.originId, dvm.adults, dvm.children)
                }
                updatePassengerText();
                disable()
            }

            function decrease(model, min) {
                if (dvm[model] > min) {
                    dvm[model]--
                }
                resolvePassengers()
            }

            function increase(model, max) {
                if (dvm[model] < max) {
                    dvm[model]++
                }
                resolvePassengers()
            }

            function disable() {
                dvm.disabled.increase = dvm.adults + dvm.children >= $scope.maxPassengers ? {
                    adults: true,
                    children: true
                } : {
                    adults: false,
                    children: false
                };
                dvm.disabled.decrease.adults = dvm.adults <= $scope.minAdults ? true : false;
                dvm.disabled.decrease.children = dvm.children <= $scope.minChildren ? true : false
            }

            function increaseChildren() {
                increase("children", $scope.maxPassengers - dvm["adults"])
            }

            function decreaseChildren() {
                decrease("children", $scope.minChildren)
            }

            function increaseAdults() {
                increase("adults", $scope.maxPassengers - dvm["children"])
            }

            function decreaseAdults() {
                decrease("adults", $scope.minAdults)
            }

            function toggleSelect() {
                dvm.isDropdownVisible = !dvm.isDropdownVisible
            }

            function hideChildren() {
                return dvm["adults"] >= $scope.maxPassengers ? true : false
            }

            function continueMobile() {
                if ($scope.checkChildrenAges()) {
                    $scope.$emit("passengersValue", {
                        adults: dvm.adults,
                        children: dvm.children
                    })
                }
            }
            dvm.increaseChildren = increaseChildren;
            dvm.decreaseChildren = decreaseChildren;
            dvm.increaseAdults = increaseAdults;
            dvm.decreaseAdults = decreaseAdults;
            dvm.hideChildren = hideChildren;
            dvm.isDropdownVisible = false;
            dvm.disabled = {
                decrease: {
                    adults: true,
                    children: true
                },
                increase: {
                    adults: false,
                    children: false
                }
            };
            dvm.toggleSelect = toggleSelect;
            dvm.continueMobile = continueMobile
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("results", results);

        function results() {
            return {
                restrict: "EA",
                controller: resultsController,
                controllerAs: "results",
                scope: {
                    url: "@"
                },
                template: function(element) {
                    return element.html()
                }
            }
        }
        resultsController.$inject = ["$scope", "$http", "$timeout", "$window", "analyticsEc"];

        function resultsController($scope, $http, $timeout, $window, analyticsEc) {
            var dvm = this,
                timeoutInterval = 6e5,
                timer, lastSearchedUrl;
            dvm.unfilteredOffers = [];
            dvm.offers = [];
            dvm.filters = {};
            dvm.loading = true;
            dvm.loadingFilters = false;
            dvm.error = false;
            dvm.offersNumber = 0;
            dvm.resultsExpired = false;
            dvm.getSegmentsLength = getSegmentsLength;
            dvm.resetFilters = resetFilters;
            dvm.areFiltersApplied = areFiltersApplied;
            dvm.showFilters = showFilters;
            dvm.redoLastSearch = redoLastSearch;
            $scope.$on("redoSearch", function($event, url) {
                getResults(url)
            });
            $scope.$on("getResultsFinished", function() {
                if (!dvm.error) {
                    timer = $timeout(function() {
                        dvm.resultsExpired = true
                    }, timeoutInterval)
                }
            });
            $scope.$on("flightResultPageChanged", function($event, pagination) {
                var offset = (pagination.currentPage - 1) * pagination.nElementsPage;
                if (dvm.offers.length > 0) {
                    trackingPage(offset, pagination.nElementsPage)
                }
            });
            $scope.$on("flight-selected", function($event, data) {
                if (dvm.offers[data.position]) {
                    trackingSelect(data.position)
                }
            });
            $scope.$on("applyFilters", applyFiltersListener);
            init();

            function init() {
                getResults($scope.url)
            }

            function resetFilters() {
                $scope.$emit("resetFilters")
            }

            function applyFiltersListener($event, filterFunction) {
                dvm.loadingFilters = true;
                $timeout(function() {
                    dvm.offers = _.filter(dvm.unfilteredOffers, filterFunction);
                    $timeout(function() {
                        $scope.$emit("resultsFiltered");
                        dvm.loadingFilters = false
                    }, 0)
                }, 500)
            }

            function showFilters() {
                $scope.$emit("closeFilters")
            }

            function redoLastSearch() {
                if (lastSearchedUrl) {
                    getResults(lastSearchedUrl)
                }
            }

            function getResults(url) {
                resetVariables();
                resetFilters();
                dvm.loading = true;
                $http.get(url).then(function(result) {
                    if (result.status < 200 || result.status >= 300) {
                        dvm.error = true;
                        return
                    }
                    try {
                        dvm.offers = dvm.unfilteredOffers = result.data.offers;
                        dvm.filters = result.data.filters;
                        dvm.offersNumber = dvm.offers.length;
                        lastSearchedUrl = url
                    } catch (e) {
                        dvm.error = true
                    }
                }).catch(function() {
                    dvm.error = true
                }).finally(function() {
                    dvm.loading = false;
                    $timeout(function() {
                        $scope.$emit("getResultsFinished", dvm.filters);
                        $scope.$emit("resultsFiltered")
                    })
                })
            }

            function getSegmentsLength(segmentsCount) {
                return segmentsCount - 1 === 0 ? 1 : segmentsCount - 1
            }

            function areFiltersApplied() {
                return dvm.unfilteredOffers && dvm.offers && dvm.unfilteredOffers.length > dvm.offers.length
            }

            function resetVariables() {
                dvm.error = false;
                dvm.offers = [];
                dvm.unfilteredOffers = [];
                dvm.filters = {};
                dvm.offersNumber = 0;
                dvm.resultsExpired = false;
                $timeout.cancel(timer)
            }

            function trackingPage(offset, elementNum) {
                var ido, result, impresion, impresions = [],
                    iteration = 0;
                if (dvm.error) {
                    return
                }
                for (ido in dvm.offers) {
                    iteration++;
                    if (iteration <= offset) {
                        continue
                    }
                    if (iteration > offset + elementNum) {
                        continue
                    }
                    result = dvm.offers[ido];
                    impresion = getImpresionData(result);
                    impresion.position = parseInt(ido) + 1;
                    impresions.push(impresion)
                }
                analyticsEc.trackImpressions(getCurrency(), impresions)
            }

            function getImpresionData(item) {
                var id = item.departureStartAirport + "-" + item.departureEndAirport;
                return {
                    id: id,
                    name: item.ec.name,
                    category: "Flights",
                    brand: item.departureAirlines.join(),
                    variant: item.ec.variant,
                    list: id + " flight results",
                    price: item.totalPrice
                }
            }

            function getCurrency() {
                if (!dvm.offers[0]) {
                    return false
                }
                return dvm.offers[0].currency
            }

            function trackingSelect(position) {
                var product = gaProduct(position);
                if (!$window.ga) {
                    return
                }
                analyticsEc.trackClick(getCurrency(), "flight", product)
            }

            function gaProduct(position) {
                var offer = dvm.offers[position],
                    product = getImpresionData(offer);
                delete product["list"];
                return product
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("resultsTopForm", ["$document", resultsTopForm]);

        function resultsTopForm($document) {
            return {
                restrict: "A",
                controller: resultsTopFormController,
                controllerAs: "resultsTopForm",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    isReturn: "@"
                },
                compile: function() {
                    return {
                        pre: function() {},
                        post: function(scope, element) {
                            function changeState(event) {
                                var isInside = element.find(event.target).length > 0;
                                scope.setSummary(!isInside);
                                if (scope.$root.$$phase !== "$apply" && scope.$root.$$phase !== "$digest") {
                                    scope.$apply()
                                }
                            }
                            $document.bind("click", changeState);
                            scope.$on("$destroy", function() {
                                $document.unbind("click", changeState)
                            })
                        }
                    }
                }
            }
        }
        resultsTopFormController.$inject = ["$rootScope", "$window", "$scope", "$timeout", "screenSize"];

        function resultsTopFormController($rootScope, $window, $scope, $timeout, screenSize) {
            var dvm = this,
                mobileScreenSize = "xs";
            dvm.loadingResults = true;
            dvm.summary = true;
            dvm.summaryToFalse = false;
            dvm.submit = submit;
            dvm.goToSearch = goToSearch;
            dvm.setIsReturn = setIsReturn;
            dvm.firstOpenDate = firstOpenDate;
            init();
            $scope.setSummary = function(state) {
                var preState = dvm.summary;
                screenSize.is(mobileScreenSize) ? dvm.summary = true : dvm.summary = state;
                dvm.summaryToFalse = preState === true && dvm.summary === false ? true : false
            };

            function firstOpenDate(step) {
                $timeout(function() {
                    if (dvm.summaryToFalse === true) {
                        $rootScope.$broadcast("openDate", step)
                    }
                })
            }
            $rootScope.$on("getResultsFinished", getResultsFinishedListener);

            function init() {
                dvm.isReturn = parseInt($scope.isReturn);
                dvm.isXs = screenSize.is(mobileScreenSize)
            }

            function getParams() {
                var params, children, ages = [];
                children = $('input[name="children"]').val();
                params = {
                    adults: $('input[name="adults"]').val(),
                    children: children,
                    f: $('input[name="f"]').val(),
                    t: $('input[name="t"]').val(),
                    d: $('input[name="d"]').val()
                };
                if (dvm.isReturn) {
                    params["r"] = $('input[name="r"]').val()
                }
                if (children !== "0") {
                    $('[name="c-a[]"]').each(function() {
                        if ($(this).val() !== "-1") {
                            ages.push($(this).val())
                        }
                    });
                    params["c-a"] = ages
                }
                return $.param(params)
            }

            function submit($event) {
                var url = angular.element($event.target).attr("action") + "?" + getParams();
                $event.preventDefault();
                if (!dvm.loadingResults) {
                    dvm.loadingResults = true;
                    $rootScope.$broadcast("redoSearch", url)
                }
            }

            function goToSearch($event, url) {
                if (url && screenSize.is(mobileScreenSize)) {
                    $event.preventDefault();
                    goTo(url)
                }
            }

            function goTo(url) {
                $window.location.href = url + "?" + getParams()
            }

            function getResultsFinishedListener() {
                var formSearch;
                if ($window.history && $window.history.pushState) {
                    formSearch = $window.location.pathname + "?" + getParams();
                    $window.history.pushState({}, "", formSearch)
                }
                dvm.loadingResults = false
            }

            function setIsReturn(is) {
                if (!screenSize.is(mobileScreenSize)) {
                    dvm.isReturn = is
                }
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("selectInputOnClick", selectInputOnClick);
        selectInputOnClick.$inject = ["$window", "$timeout"];

        function selectInputOnClick($window, $timeout) {
            var directive = {
                restrict: "A",
                scope: {},
                link: linkFunc
            };
            return directive;

            function linkFunc(scope, element) {
                element.on("click", function() {
                    var $inputs = element.find('input[type="text"]'),
                        input;
                    input = $inputs.length > 0 ? $inputs[0] : null;
                    if (input && input.value && !$window.getSelection().toString()) {
                        $timeout(function() {
                            input.focus();
                            input.setSelectionRange(0, input.value.length)
                        }, 1)
                    }
                })
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("validateConditions", [function() {
            return {
                restrict: "A",
                require: "ngModel",
                link: function(scope, element, attrs, ctrl) {
                    ctrl.$parsers.unshift(isChecked);
                    ctrl.$formatters.unshift(isChecked);

                    function isChecked(value) {
                        ctrl.$setValidity("conditions", value);
                        return value
                    }
                }
            }
        }])
    })();
    (function() {
        "use strict";
        angular.module("app").directive("validateEmail", [function() {
            return {
                restrict: "A",
                require: "ngModel",
                link: function(scope, element, attrs, ctrl) {
                    var pattern = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}/;
                    ctrl.$validators.email = function(modelValue, viewValue) {
                        var value = modelValue || viewValue;
                        return pattern.test(value)
                    }
                }
            }
        }])
    })();
    (function() {
        "use strict";
        angular.module("app").service("flightOffersFilter", flightOffersFilter);

        function flightOffersFilter() {
            var dvm = this;
            dvm.props = {};
            dvm.setInitialProps = function() {
                dvm.props = {
                    direct: false,
                    "1stop": false,
                    "2stops": false,
                    departureTime: false,
                    returnTime: false,
                    duration: false,
                    airlines: {},
                    airports: {
                        from: {},
                        to: {}
                    }
                }
            };
            dvm.setInitialProps();
            dvm.set = function(prop, value) {
                dvm.props[prop] = value
            };
            dvm.reset = function() {
                dvm.setInitialProps()
            };
            dvm.setDictionary = function(keys, prop, value) {
                var element = dvm.props;
                _.each(keys, function(key) {
                    if (!element.hasOwnProperty(key)) {
                        element[key] = {}
                    }
                    element = element[key]
                });
                element[prop] = value
            };
            dvm.filterOffer = function(offer) {
                if (!dvm.filterStops(offer)) {
                    return false
                }
                if (!dvm.filterDuration(offer)) {
                    return false
                }
                if (!dvm.departureTime(offer)) {
                    return false
                }
                if (!offer.isOneWay && !dvm.returnTime(offer)) {
                    return false
                }
                if (!dvm.filterStartAirport(offer)) {
                    return false
                }
                if (!dvm.filterEndAirport(offer)) {
                    return false
                }
                if (!dvm.filterAirlines(offer)) {
                    return false
                }
                if (!dvm.isSameAirport(offer)) {
                    return false
                }
                return true
            };
            dvm.departureTime = function(offer) {
                var departureHours;
                if (!dvm.props.departureTime) {
                    return true
                }
                departureHours = dvm.timeToHours(offer.departureTime);
                if (dvm.props.departureTime.min > departureHours) {
                    return false
                }
                if (dvm.props.departureTime.max < departureHours) {
                    return false
                }
                return true
            };
            dvm.returnTime = function(offer) {
                var returnHours;
                if (!dvm.props.returnTime) {
                    return true
                }
                returnHours = dvm.timeToHours(offer.returnTime);
                if (dvm.props.returnTime.min > returnHours) {
                    return false
                }
                if (dvm.props.returnTime.max < returnHours) {
                    return false
                }
                return true
            };
            dvm.filterDuration = function(offer) {
                var departureHours, returnHours;
                if (typeof dvm.props.duration !== "object") {
                    return true
                }
                departureHours = dvm.timeToHours(offer.departureDuration);
                returnHours = !offer.isOneWay ? dvm.timeToHours(offer.returnDuration) : null;
                if (dvm.props.duration.min > departureHours || !offer.isOneWay && dvm.props.duration.min > returnHours) {
                    return false
                }
                if (dvm.props.duration.max < departureHours || !offer.isOneWay && dvm.props.duration.max < returnHours) {
                    return false
                }
                return true
            };
            dvm.timeToHours = function(strTime) {
                var time, hours, minutes;
                time = strTime.split(":");
                hours = parseInt(time[0], 10);
                minutes = parseInt(time[1], 10);
                return hours + minutes / 60
            };
            dvm.filterStops = function(offer) {
                var anyStop = dvm.props["direct"] || dvm.props["1stop"] || dvm.props["2stops"];
                if (dvm.props["direct"] && (offer.departureNStops === 0 && (offer.isOneWay || offer.returnNStops === 0))) {
                    return true
                }
                if (dvm.props["1stop"] && (offer.departureNStops === 1 && (offer.isOneWay || offer.returnNStops <= 1) || !offer.isOneWay && offer.returnNStops === 1 && offer.departureNStops <= 1)) {
                    return true
                }
                if (dvm.props["2stops"] && (offer.departureNStops >= 2 || !offer.isOneWay && offer.returnNStops >= 2)) {
                    return true
                }
                return !anyStop
            };
            dvm.filterAirlines = function(offer) {
                var result = false,
                    anyActive = false;
                _.each(dvm.props.airlines, function(active, airline) {
                    if (active && (_.contains(offer.departureAirlines, airline) || _.contains(offer.returnAirlines, airline))) {
                        result = true
                    } else if (active) {
                        anyActive = true
                    }
                });
                return result || !anyActive
            };
            dvm.filterStartAirport = function(offer) {
                return dvm.filterAirport(dvm.props.airports.from, offer.departureStartAirport) && (dvm.filterAirport(dvm.props.airports.from, offer.returnEndAirport) || offer.isOneWay)
            };
            dvm.filterEndAirport = function(offer) {
                return dvm.filterAirport(dvm.props.airports.to, offer.departureEndAirport) && (dvm.filterAirport(dvm.props.airports.to, offer.returnStartAirport) || offer.isOneWay)
            };
            dvm.filterAirport = function(airports, airportToCheck) {
                var result = false,
                    anyActive = false;
                _.each(airports, function(active, airport) {
                    if (active && airport === airportToCheck) {
                        result = true
                    } else if (active) {
                        anyActive = true
                    }
                });
                return result || !anyActive
            };
            dvm.isSameAirport = function(offer) {
                var result = !dvm.props.isSameAirport;
                if (dvm.props.isSameAirport && (offer.isOneWay || offer.departureStartAirport === offer.returnEndAirport && offer.departureEndAirport === offer.returnStartAirport)) {
                    result = true
                }
                return result
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").service("LocationSuggestionService", LocationSuggestionService);
        LocationSuggestionService.$inject = ["$http", "consts"];

        function LocationSuggestionService($http, consts) {
            this.searchAirport = searchAirport;
            this.searchCity = searchCity;

            function searchAirport(text) {
                var url = consts.airports_autocomplete;
                return search(url, text)
            }

            function searchCity(text) {
                var url = consts.cities_autocomplete;
                return search(url, text)
            }

            function search(url, text) {
                return $http.get(url, {
                    params: {
                        text: text
                    },
                    cache: true
                })
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").service("analyticsEc", analyticsEc);
        analyticsEc.$inject = ["$location", "Utils", "$window"];

        function analyticsEc($location, Utils, $window) {
            var dvm = this;
            dvm.trackPageview = trackPageview;
            dvm.trackImpressions = trackImpressions;
            dvm.trackClick = trackClick;
            dvm.trackCheckout = trackCheckout;
            dvm.trackPurchase = trackPurchase;

            function trackPageview(currency, gaProduct, step) {
                if (!$window.ga || isRefreshed()) {
                    return false
                }
                $window.ga("set", "currencyCode", currency);
                $window.ga("ec:addProduct", gaProduct);
                $window.ga("ec:setAction", "checkout", {
                    step: step
                });
                $window.ga("send", "pageview", $location.url())
            }

            function trackImpressions(currency, impresions) {
                var idx;
                if (!$window.ga) {
                    return false
                }
                $window.ga("set", "currencyCode", currency);
                for (idx in impresions) {
                    $window.ga("ec:addImpression", impresions[idx])
                }
                $window.ga("send", "pageview", $location.url())
            }

            function trackClick(currency, category, properties) {
                if (!$window.ga) {
                    return false
                }
                $window.ga("set", "currencyCode", currency);
                $window.ga("ec:addProduct", properties);
                $window.ga("ec:setAction", "click", {
                    list: properties.id + " " + category + " results"
                });
                $window.ga("send", "event", category, "click")
            }

            function trackCheckout(currency, gaProduct, properties) {
                if (!$window.ga) {
                    return false
                }
                $window.ga("set", "currencyCode", currency);
                $window.ga("ec:addProduct", gaProduct);
                $window.ga("ec:setAction", "checkout", properties);
                $window.ga("send", "event", "UX", "submit", properties.step)
            }

            function trackPurchase(currency, gaProduct, properties, scopeId) {
                if (!$window.ga || Utils.getCookie(scopeId)) {
                    return false
                }
                $window.ga("set", "currencyCode", currency);
                $window.ga("ec:addProduct", gaProduct);
                $window.ga("ec:setAction", "purchase", properties);
                $window.ga("send", "pageview", $location.url(), {
                    hitCallback: function() {
                        Utils.setCookie(scopeId)
                    }
                })
            }

            function isRefreshed() {
                return $window.performance && $window.performance.navigation.type === 1
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("tripDetails", tripDetails);

        function tripDetails() {
            return {
                restrict: "A",
                controller: tripDetailsController,
                controllerAs: "tripDetails",
                template: function(element) {
                    return element.html()
                },
                scope: {
                    detailsUrl: "@"
                }
            }
        }
        tripDetailsController.$inject = ["$scope", "$element", "$timeout", "$http"];

        function tripDetailsController($scope, $element, $timeout, $http) {
            var dvm = this,
                openClass = "open",
                container = $element.find(".details-container"),
                detailsUrl = $scope.detailsUrl,
                timeout = 400;
            dvm.itemsRetrieved = false;
            dvm.show = false;
            dvm.showDetails = showDetails;

            function showDetails() {
                if ($element.hasClass(openClass)) {
                    $element.removeClass(openClass);
                    container.slideUp(timeout)
                } else {
                    dvm.show = true;
                    if (dvm.itemsRetrieved === false) {
                        getDetails()
                    }
                    $element.addClass(openClass);
                    container.slideDown(timeout)
                }
            }

            function getDetails() {
                $http.get(detailsUrl).then(function(response) {
                    processResult(response.data)
                }, function(response) {
                    dvm.error = true
                })
            }

            function processResult(html) {
                dvm.itemsRetrieved = true;
                $($.parseHTML(html)).appendTo(container)
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("tripsOverview", tripsOverview);

        function tripsOverview() {
            return {
                restrict: "A",
                controller: tripsOverviewController,
                controllerAs: "tripsOverview",
                scope: {
                    url: "@"
                },
                template: function(element) {
                    return element.html()
                }
            }
        }
        tripsOverviewController.$inject = ["$scope", "$http", "$timeout"];

        function tripsOverviewController($scope, $http, $timeout) {
            var dvm = this;
            dvm.upcoming = [];
            dvm.all = [];
            dvm.view = [];
            dvm.loaded = false;
            dvm.error = false;
            dvm.showTrips = showTrips;
            dvm.getResults = getResults;
            init();

            function init() {
                getResults($scope.url)
            }

            function getResults(url) {
                $http.get(url).then(function(result) {
                    if (result.status < 200 || result.status >= 300) {
                        dvm.error = true;
                        return
                    }
                    try {
                        dvm.upcoming = result.data.upcoming;
                        dvm.all = result.data.all
                    } catch (e) {
                        dvm.error = true
                    }
                }).catch(function() {
                    dvm.error = true
                }).finally(function() {
                    showTrips("all");
                    dvm.loaded = true
                })
            }

            function showTrips(type) {
                dvm.type = type;
                if (type === "all") {
                    dvm.view = dvm.all
                } else {
                    dvm.view = dvm.upcoming
                }
                $timeout(function() {
                    $timeout(function() {
                        $scope.$emit("resultsFiltered")
                    }, 0)
                }, 500)
            }
        }
    })();
    (function() {
        "use strict";
        angular.module("app").directive("emailValid", emailValid);

        function emailValid() {
            return {
                restrict: "A",
                controller: emailValidController,
                controllerAs: "emailValid",
                template: function(element) {
                    return element.html()
                },
                scope: {}
            }
        }
        emailValidController.$inject = ["$element"];

        function emailValidController($element) {
            var emailValid = this,
                input = $element.find("input"),
                placeholder = $element.find("label");
            emailValid.isValid = isValid;

            function isEmpty(field) {
                return !field.val()
            }

            function isValid($event) {
                if (!isEmpty(input) && !$event.target.validity.valid) {
                    placeholder.addClass("down")
                } else {
                    placeholder.removeClass("down")
                }
            }
        }
    })();
    (function($) {
        "use strict";
        angular.module("app").directive("scrollTo", scrollTo);

        function scrollTo() {
            return {
                restrict: "A",
                controller: scrollToController,
                controllerAs: "scrollTo",
                template: function(element) {
                    return element.html()
                },
                scope: {}
            }
        }
        scrollToController.$inject = ["$scope"];

        function scrollToController($scope) {
            var scrollTo = this,
                $body = $("html, body");
            scrollTo.click = click;

            function changeOffsetOnScroll(event, yOffset) {
                scrollTo.scrolled = yOffset >= 80;
                if ($scope.$root.$$phase !== "$apply" && $scope.$root.$$phase !== "$digest") {
                    $scope.$apply()
                }
            }

            function click(target) {
                var offset;
                offset = scrollTo.scrolled ? 60 : 100;
                $body.animate({
                    scrollTop: angular.element(target).offset().top - offset
                }, 200)
            }
            $scope.$on("scrolling", changeOffsetOnScroll)
        }
    })(jQuery);
    (function() {
        "use strict";
        angular.module("app").directive("waitUntilGmapsIsReady", waitUntilGmapsIsReady);
        waitUntilGmapsIsReady.$inject = ["$compile", "$window"];

        function waitUntilGmapsIsReady($compile, $window) {
            return {
                restrict: "A",
                replace: false,
                terminal: true,
                priority: 100,
                link: function link(scope, element) {
                    (function compile() {
                        if ($window.google && $window.google.maps) {
                            element.removeAttr("data-wait-until-gmaps-is-ready");
                            $compile(element)(scope)
                        } else {
                            setTimeout(compile, 100)
                        }
                    })()
                }
            }
        }
    })();

    $(document).ready(function() {
        angular.bootstrap(angular.element("#main-app"), ["app"]);
    });
}(function() {
    function isAngularLoader() {
        if (typeof angular == 'object') {
            asyncAppBootstrap();
            return;
        }
        setTimeout(isAngularLoader, 50);
    }
    isAngularLoader();
})();
