(function() {
    "use strict";
    angular.module("myApp").factory("Tareas", function() {
      var tareas = [];
        return {
          get: get,
          add: add,
          remove: remove,
          cambiarEstado: cambiarEstado,
          reset: reset
        };

        function get() {
          return tareas;
        }

        function add(tarea) {
          tareas.push(tarea);
        }

        function remove(index) {
          tareas.splice(index, 1);
        }

        function cambiarEstado(index) {
          var tarea = tareas[index];
          tarea.completado = !tarea.completado;
        }

        function reset() {
          tareas = [];
        }

    })
})();
