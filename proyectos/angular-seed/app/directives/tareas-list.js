(function(){
  'use strict';

  angular.module("myApp").directive("tareasList", tareasList);

  function tareasList() {
      return {
          restrict: "A",
          controller: tareasListController,
          controllerAs: "tareasList",
          template: function(element) {
              return element.html()
          },
          scope: {}
      }
  }
  tareasListController.$inject = ["$scope", "Tareas"];

  function tareasListController($scope, Tareas) {

    var tareasList = this;

    this.tareas = Tareas.get();

    this.cambiarEstado = function(index) {
      Tareas.cambiarEstado(index);
    };

    this.remove = function(index) {
      Tareas.remove(index);
    };

    this.reset = function() {
      Tareas.reset();
    };

  }

})();
