(function(){
  'use strict';

  angular.module("myApp").directive("tareaForm", tareaForm);

  function tareaForm() {
      return {
          restrict: "A",
          controller: tareaFormController,
          controllerAs: "tareaForm",
          template: function(element) {
              return element.html()
          },
          scope: {}
      }
  }
  tareaFormController.$inject = ["$scope", "Tareas"];

  function tareaFormController($scope, Tareas) {

    var tareaForm = this;

    this.saveTarea = function (saveTareaForm) {
      if (!saveTareaForm.descripcion.$valid) {
        return;
      }

      var tarea = {
        'titulo': tareaForm.titulo,
        'descripcion': tareaForm .descripcion,
        'completado': false,
        'fecha': new Date()
      };

      Tareas.add(tarea);

      console.log('tareas >>> ', Tareas.get());

    };

  }

})();
